package com.juank.proyecto.modelo;

import java.util.ArrayList;
import java.util.List;

/**
 * Clase que representa cada una de las partes de un menu y sus platos.
 * El servicio envía una lista de objetos MenuREST en el que cada elemento de corresponde con
 * una sección del menú (primero, segundo o postre)
 */
public class MenuREST {

    private String tipo;

    private List<String> platos= new ArrayList<>();

    public List<String> getPlatos() {
        return platos;
    }

    public void setPlatos(List<String> platos) {
        this.platos = platos;
    }
    public MenuREST(String tipo, List<String> platos) {
        this.tipo = tipo;
        this.platos=platos;
    }

    public MenuREST() {
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }





}
