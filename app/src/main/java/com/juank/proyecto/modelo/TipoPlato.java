
package com.juank.proyecto.modelo;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Clase para representar un tipo de plato
 */
public class TipoPlato implements Serializable
{

    @SerializedName("idTipo")
    @Expose
    private int idTipo;
    @SerializedName("tipo")
    @Expose
    private String tipo;
    private final static long serialVersionUID = 2856096477769357163L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public TipoPlato() {
    }

    /**
     * 
     * @param tipo Tipo de plato
     * @param idTipo id del tipo
     */
    public TipoPlato(int idTipo, String tipo) {
        super();
        this.idTipo = idTipo;
        this.tipo = tipo;
    }

    public int getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(int idTipo) {
        this.idTipo = idTipo;
    }

    public TipoPlato withIdTipo(int idTipo) {
        this.idTipo = idTipo;
        return this;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public TipoPlato withTipo(String tipo) {
        this.tipo = tipo;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("idTipo", idTipo).append("tipo", tipo).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(tipo).append(idTipo).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof TipoPlato) == false) {
            return false;
        }
        TipoPlato rhs = ((TipoPlato) other);
        return new EqualsBuilder().append(tipo, rhs.tipo).append(idTipo, rhs.idTipo).isEquals();
    }

}
