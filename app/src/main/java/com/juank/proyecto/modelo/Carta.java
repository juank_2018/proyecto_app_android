
package com.juank.proyecto.modelo;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Clase para representar una carta obtenida desde el JSON que envía el servicio REST.-
 */
public class Carta implements Serializable
{

    @SerializedName("idcarta")
    @Expose
    private int idcarta;
    @SerializedName("noombre")
    @Expose
    private String noombre;
    private final static long serialVersionUID = 2658344273907801124L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Carta() {
    }

    /**
     * 
     * @param idcarta id de la carta
     * @param noombre Nombre de la carta
     */
    public Carta(int idcarta, String noombre) {
        super();
        this.idcarta = idcarta;
        this.noombre = noombre;
    }

    /**
     * Obtiene el id de la carta
     * @return un entero con el id de la carta
     */
    public int getIdcarta() {
        return idcarta;
    }

    /**
     *
     * @param idcarta entero con el id de la carta
     */
    public void setIdcarta(int idcarta) {
        this.idcarta = idcarta;
    }


    /**
     *
     * @return devuelve el nombre de la carta
     */
    public String getNoombre() {
        return noombre;
    }

    /**
     *
     * @param noombre string con el nombre para la carta
     */
    public void setNoombre(String noombre) {
        this.noombre = noombre;
    }



    @Override
    public String toString() {
        return new ToStringBuilder(this).append("idcarta", idcarta).append("noombre", noombre).toString();
    }

}
