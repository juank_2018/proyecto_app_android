
package com.juank.proyecto.modelo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Clase para representar cada una de las partes de la carta
 * (entrantes, carnes, pescados...)
 */
public class SeccionCarta implements Serializable
{

    @SerializedName("tipoPlato")
    @Expose
    private String tipoPlato;
    @SerializedName("idTipoPlato")
    @Expose
    private int idTipoPlato;
    @SerializedName("platosSeccion")
    @Expose
    @Valid
    private ArrayList<Plato> platosSeccion = new ArrayList<>();
    private final static long serialVersionUID = -9214316640001608156L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public SeccionCarta() {
    }

    /**
     *
     * @param tipoPlato Tipo de plato (carne, entrante...)
     * @param platosSeccion Lista de platos de cada tipo
     * @param idTipoPlato id del tipo de plato
     */
    public SeccionCarta(String tipoPlato, int idTipoPlato, ArrayList<Plato> platosSeccion) {
        super();
        this.tipoPlato = tipoPlato;
        this.idTipoPlato = idTipoPlato;
        this.platosSeccion = platosSeccion;
    }

    public String getTipoPlato() {
        return tipoPlato;
    }

    public void setTipoPlato(String tipoPlato) {
        this.tipoPlato = tipoPlato;
    }

    public SeccionCarta withTipoPlato(String tipoPlato) {
        this.tipoPlato = tipoPlato;
        return this;
    }

    public int getIdTipoPlato() {
        return idTipoPlato;
    }

    public void setIdTipoPlato(int idTipoPlato) {
        this.idTipoPlato = idTipoPlato;
    }

    public SeccionCarta withIdTipoPlato(int idTipoPlato) {
        this.idTipoPlato = idTipoPlato;
        return this;
    }

    public ArrayList<Plato> getPlatosSeccion() {
        return platosSeccion;
    }

    public void setPlatosSeccion(ArrayList<Plato> platosSeccion) {
        this.platosSeccion = platosSeccion;
    }

    public SeccionCarta withPlatosSeccion(ArrayList<Plato> platosSeccion) {
        this.platosSeccion = platosSeccion;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("tipoPlato", tipoPlato).append("idTipoPlato", idTipoPlato).append("platosSeccion", platosSeccion).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(tipoPlato).append(platosSeccion).append(idTipoPlato).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof SeccionCarta) == false) {
            return false;
        }
        SeccionCarta rhs = ((SeccionCarta) other);
        return new EqualsBuilder().append(tipoPlato, rhs.tipoPlato).append(platosSeccion, rhs.platosSeccion).append(idTipoPlato, rhs.idTipoPlato).isEquals();
    }

}
