
package com.juank.proyecto.modelo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Clase para representar una reserva
 */
public class Reserva {

    @SerializedName("idreservas")
    @Expose
    private Integer idreservas;
    @SerializedName("nComensales")
    @Expose
    private Integer nComensales;
    @SerializedName("turno")
    @Expose
    private String turno;
    @SerializedName("fechaHora")
    @Expose
    private String fechaHora;
    @SerializedName("cliente")
    @Expose
    private Cliente cliente;

    /**
     * No args constructor for use in serialization
     *
     */
    public Reserva() {
    }

    /**
     *
     * @param nComensales número de comensales
     * @param cliente Cliente que hace la reserva
     * @param fechaHora fecha y hora de la reserva
     * @param turno turno de la reserva (almuerzo, cena)
     * @param idreservas id de la reserva
     */
    public Reserva(Integer idreservas, Integer nComensales, String turno, String fechaHora, Cliente cliente) {
        super();
        this.idreservas = idreservas;
        this.nComensales = nComensales;
        this.turno = turno;
        this.fechaHora = fechaHora;
        this.cliente = cliente;
    }

    public Integer getIdreservas() {
        return idreservas;
    }

    public void setIdreservas(Integer idreservas) {
        this.idreservas = idreservas;
    }

    public Reserva withIdreservas(Integer idreservas) {
        this.idreservas = idreservas;
        return this;
    }

    public Integer getNComensales() {
        return nComensales;
    }

    public void setNComensales(Integer nComensales) {
        this.nComensales = nComensales;
    }

    public Reserva withNComensales(Integer nComensales) {
        this.nComensales = nComensales;
        return this;
    }

    public String getTurno() {
        return turno;
    }

    public void setTurno(String turno) {
        this.turno = turno;
    }

    public Reserva withTurno(String turno) {
        this.turno = turno;
        return this;
    }

    public String getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(String fechaHora) {
        this.fechaHora = fechaHora;
    }

    public Reserva withFechaHora(String fechaHora) {
        this.fechaHora = fechaHora;
        return this;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Reserva withCliente(Cliente cliente) {
        this.cliente = cliente;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("idreservas", idreservas).append("nComensales", nComensales).append("turno", turno).append("fechaHora", fechaHora).append("cliente", cliente).toString();
    }

}