package com.juank.proyecto.modelo;

/**
 * Created by jcpm0 on 18/03/2018.
 */

import com.google.gson.annotations.SerializedName;

/**
 * Representa una respuesta sencilla desde el servicio
 */
public class SimpleResponse {

    @SerializedName("mensaje")
    private String message;

    @SerializedName("error")
    private boolean error;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

}