
package com.juank.proyecto.modelo;

import java.io.Serializable;
import javax.validation.Valid;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Clase para representar un plato de la carta
 */
public class PlatoCarta implements Serializable
{

    @SerializedName("aparece")
    @Expose
    private int aparece;
    @SerializedName("plato")
    @Expose
    @Valid
    private Plato plato;
    @SerializedName("carta")
    @Expose
    @Valid
    private Carta carta;
    private final static long serialVersionUID = 1705733812934174344L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public PlatoCarta() {
    }

    /**
     * 
     * @param carta
     * @param aparece
     * @param plato
     */
    public PlatoCarta(int aparece, Plato plato, Carta carta) {
        super();
        this.aparece = aparece;
        this.plato = plato;
        this.carta = carta;
    }

    public int getAparece() {
        return aparece;
    }

    public void setAparece(int aparece) {
        this.aparece = aparece;
    }

    public PlatoCarta withAparece(int aparece) {
        this.aparece = aparece;
        return this;
    }

    public Plato getPlato() {
        return plato;
    }

    public void setPlato(Plato plato) {
        this.plato = plato;
    }

    public PlatoCarta withPlato(Plato plato) {
        this.plato = plato;
        return this;
    }

    public Carta getCarta() {
        return carta;
    }

    public void setCarta(Carta carta) {
        this.carta = carta;
    }

    public PlatoCarta withCarta(Carta carta) {
        this.carta = carta;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("aparece", aparece).append("plato", plato).append("carta", carta).toString();
    }

}
