
package com.juank.proyecto.modelo;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Clase para representar un objeto notificación
 */
public class Notificacion {

    @SerializedName("idnotificaciones")
    @Expose
    private Integer idnotificaciones;
    @SerializedName("mensaje")
    @Expose
    private String mensaje;
    @SerializedName("fecha")
    @Expose
    private String fecha;
    @SerializedName("dispositivo")
    @Expose
    private List<Dispositivo> dispositivo = null;
    @SerializedName("entregada")
    @Expose
    private Integer entregada;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Notificacion() {
    }

    /**
     * 
     * @param fecha fecha de la notificación
     * @param idnotificaciones id de la notificacion
     * @param dispositivo Lista de dispositivos
     * @param entregada indica si la notificación está entregada o no (1 entregada, 0 no entregada)
     * @param mensaje El mensaje de la notificación
     */
    public Notificacion(Integer idnotificaciones, String mensaje, String fecha, List<Dispositivo> dispositivo, Integer entregada) {
        super();
        this.idnotificaciones = idnotificaciones;
        this.mensaje = mensaje;
        this.fecha = fecha;
        this.dispositivo = dispositivo;
        this.entregada = entregada;
    }

    public Integer getIdnotificaciones() {
        return idnotificaciones;
    }

    public void setIdnotificaciones(Integer idnotificaciones) {
        this.idnotificaciones = idnotificaciones;
    }

    public Notificacion withIdnotificaciones(Integer idnotificaciones) {
        this.idnotificaciones = idnotificaciones;
        return this;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public Notificacion withMensaje(String mensaje) {
        this.mensaje = mensaje;
        return this;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public Notificacion withFecha(String fecha) {
        this.fecha = fecha;
        return this;
    }

    public List<Dispositivo> getDispositivo() {
        return dispositivo;
    }

    public void setDispositivo(List<Dispositivo> dispositivo) {
        this.dispositivo = dispositivo;
    }

    public Notificacion withDispositivo(List<Dispositivo> dispositivo) {
        this.dispositivo = dispositivo;
        return this;
    }

    public Integer getEntregada() {
        return entregada;
    }

    public void setEntregada(Integer entregada) {
        this.entregada = entregada;
    }

    public Notificacion withEntregada(Integer entregada) {
        this.entregada = entregada;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("idnotificaciones", idnotificaciones).append("mensaje", mensaje).append("fecha", fecha).append("dispositivo", dispositivo).append("entregada", entregada).toString();
    }

}
