
package com.juank.proyecto.modelo;

import java.io.Serializable;
import javax.validation.Valid;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Clase para representar un objeto Empleado.
 */
public class Empleado implements Serializable
{

    @SerializedName("idempleados")
    @Expose
    private int idempleados;
    @SerializedName("login")
    @Expose
    private String login;
    @SerializedName("paswd")
    @Expose
    private String paswd;
    @SerializedName("rol")
    @Expose
    private String rol;
    @SerializedName("usuario")
    @Expose
    @Valid
    private Usuario usuario;
    private final static long serialVersionUID = -6782283798451533620L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Empleado() {
    }

    /**
     * 
     * @param idempleados id del empleado
     * @param usuario Objeto Usuario con los datos de usuario del empleado
     * @param rol rol que tiene en el sistema
     * @param login login en la web
     * @param paswd contraseña para la web
     */
    public Empleado(int idempleados, String login, String paswd, String rol, Usuario usuario) {
        super();
        this.idempleados = idempleados;
        this.login = login;
        this.paswd = paswd;
        this.rol = rol;
        this.usuario = usuario;
    }

    public int getIdempleados() {
        return idempleados;
    }

    public void setIdempleados(int idempleados) {
        this.idempleados = idempleados;
    }

    public Empleado withIdempleados(int idempleados) {
        this.idempleados = idempleados;
        return this;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public Empleado withLogin(String login) {
        this.login = login;
        return this;
    }

    public String getPaswd() {
        return paswd;
    }

    public void setPaswd(String paswd) {
        this.paswd = paswd;
    }

    public Empleado withPaswd(String paswd) {
        this.paswd = paswd;
        return this;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public Empleado withRol(String rol) {
        this.rol = rol;
        return this;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Empleado withUsuario(Usuario usuario) {
        this.usuario = usuario;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("idempleados", idempleados).append("login", login).append("paswd", paswd).append("rol", rol).append("usuario", usuario).toString();
    }

}
