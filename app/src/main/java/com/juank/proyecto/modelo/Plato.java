
package com.juank.proyecto.modelo;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import java.io.Serializable;
import java.util.List;
import javax.validation.Valid;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Clase para representar un objeto plato.
 * implementa la interfaz parcelable para poder usarlo como argumento en CartaDetalleFragment
 */
public class Plato implements Serializable,Comparable<Plato>, Parcelable {

    @SerializedName("idPlato")
    @Expose
    private int idPlato;
    @SerializedName("nombre")
    @Expose
    private String nombre;
    @SerializedName("precioTapa")
    @Expose
    private double precioTapa;
    @SerializedName("precioMedia")
    @Expose
    private double precioMedia;
    @SerializedName("precioRacion")
    @Expose
    private double precioRacion;
    @SerializedName("tipo_plato")
    @Expose
    @Valid
    private List<TipoPlato> tipoPlato = null;
    @SerializedName("empleado")
    @Expose
    @Valid
    private Empleado empleado;
    private final static long serialVersionUID = -2910500616364331616L;
    protected Plato(Parcel in) {
        this.idPlato = ((int) in.readValue((int.class.getClassLoader())));
        this.nombre = ((String) in.readValue((String.class.getClassLoader())));
        this.precioTapa = ((int) in.readValue((int.class.getClassLoader())));
        this.precioMedia = ((int) in.readValue((int.class.getClassLoader())));
        this.precioRacion = ((int) in.readValue((int.class.getClassLoader())));
        in.readList(this.tipoPlato, (com.juank.proyecto.modelo.TipoPlato.class.getClassLoader()));
        this.empleado = ((Empleado) in.readValue((Empleado.class.getClassLoader())));
    }
    /**
     * No args constructor for use in serialization
     * 
     */
    public Plato()  {
    }

    /**
     * 
     * @param nombre nombre del plato
     * @param tipoPlato Lista con los tipos de plato
     * @param empleado empleado que ha creado el plato
     * @param precioMedia precio media ración
     * @param idPlato id del plato
     * @param precioRacion precio de la ración
     * @param precioTapa precio tapa
     */
    public Plato(int idPlato, String nombre, double precioTapa, double precioMedia, double precioRacion, List<TipoPlato> tipoPlato, Empleado empleado)  {
        super();
        this.idPlato = idPlato;
        this.nombre = nombre;
        this.precioTapa = precioTapa;
        this.precioMedia = precioMedia;
        this.precioRacion = precioRacion;
        this.tipoPlato = tipoPlato;
        this.empleado = empleado;
    }

    public static final Creator<Plato> CREATOR = new Creator<Plato>() {
        @Override
        public Plato createFromParcel(Parcel in) {
            return new Plato(in);
        }

        @Override
        public Plato[] newArray(int size) {
            return new Plato[size];
        }
    };

    public int getIdPlato() {
        return idPlato;
    }

    public void setIdPlato(int idPlato) {
        this.idPlato = idPlato;
    }

    public Plato withIdPlato(int idPlato) {
        this.idPlato = idPlato;
        return this;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Plato withNombre(String nombre) {
        this.nombre = nombre;
        return this;
    }

    public double getPrecioTapa() {
        return precioTapa;
    }

    public void setPrecioTapa(double precioTapa) {
        this.precioTapa = precioTapa;
    }

    public Plato withPrecioTapa(double precioTapa) {
        this.precioTapa = precioTapa;
        return this;
    }

    public double getPrecioMedia() {
        return precioMedia;
    }

    public void setPrecioMedia(double precioMedia) {
        this.precioMedia = precioMedia;
    }

    public Plato withPrecioMedia(double precioMedia) {
        this.precioMedia = precioMedia;
        return this;
    }

    public double getPrecioRacion() {
        return precioRacion;
    }

    public void setPrecioRacion(double precioRacion) {
        this.precioRacion = precioRacion;
    }

    public Plato withPrecioRacion(double precioRacion) {
        this.precioRacion = precioRacion;
        return this;
    }

    public List<TipoPlato> getTipoPlato() {
        return tipoPlato;
    }

    public void setTipoPlato(List<TipoPlato> tipoPlato) {
        this.tipoPlato = tipoPlato;
    }

    public Plato withTipoPlato(List<TipoPlato> tipoPlato) {
        this.tipoPlato = tipoPlato;
        return this;
    }

    public Empleado getEmpleado() {
        return empleado;
    }

    public void setEmpleado(Empleado empleado) {
        this.empleado = empleado;
    }

    public Plato withEmpleado(Empleado empleado) {
        this.empleado = empleado;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("idPlato", idPlato).append("nombre", nombre).append("precioTapa", precioTapa).append("precioMedia", precioMedia).append("precioRacion", precioRacion).append("tipoPlato", tipoPlato).append("empleado", empleado).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(nombre).append(tipoPlato).append(empleado).append(precioMedia).append(idPlato).append(precioRacion).append(precioTapa).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Plato) == false) {
            return false;
        }
        Plato rhs = ((Plato) other);
        return new EqualsBuilder().append(nombre, rhs.nombre).append(tipoPlato, rhs.tipoPlato).append(empleado, rhs.empleado).append(precioMedia, rhs.precioMedia).append(idPlato, rhs.idPlato).append(precioRacion, rhs.precioRacion).append(precioTapa, rhs.precioTapa).isEquals();
    }

    @Override
    public int compareTo(@NonNull Plato plato) {
        if(!tipoPlato.isEmpty()){
            if(tipoPlato.get(0).getIdTipo() < plato.tipoPlato.get(0).getIdTipo()){
                return -1;
            }
            if (tipoPlato.get(0).getIdTipo() > plato.tipoPlato.get(0).getIdTipo()){
                return 1;
            }
        }

        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(idPlato);
        dest.writeValue(nombre);
        dest.writeValue(precioTapa);
        dest.writeValue(precioMedia);
        dest.writeValue(precioRacion);
        dest.writeList(tipoPlato);
        dest.writeValue(empleado);
    }

    public int describeContents() {
        return 0;
    }
}
