
package com.juank.proyecto.modelo;

import java.io.Serializable;
import javax.validation.Valid;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class PlatoMenu implements Serializable
{

    @SerializedName("tipo")
    @Expose
    private String tipo;
    @SerializedName("plato")
    @Expose
    @Valid
    private Plato plato;
    @SerializedName("menu")
    @Expose
    @Valid
    private Menu menu;
    private final static long serialVersionUID = -539147412576031801L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public PlatoMenu() {
    }

    /**
     * 
     * @param menu
     * @param tipo
     * @param plato
     */
    public PlatoMenu(String tipo, Plato plato, Menu menu) {
        super();
        this.tipo = tipo;
        this.plato = plato;
        this.menu = menu;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public PlatoMenu withTipo(String tipo) {
        this.tipo = tipo;
        return this;
    }

    public Plato getPlato() {
        return plato;
    }

    public void setPlato(Plato plato) {
        this.plato = plato;
    }

    public PlatoMenu withPlato(Plato plato) {
        this.plato = plato;
        return this;
    }

    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    public PlatoMenu withMenu(Menu menu) {
        this.menu = menu;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("tipo", tipo).append("plato", plato).append("menu", menu).toString();
    }

}
