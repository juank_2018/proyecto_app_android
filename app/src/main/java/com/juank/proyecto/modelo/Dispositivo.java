
package com.juank.proyecto.modelo;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Clase para representar un objeto Dispositivo.
 */
public class Dispositivo implements Serializable
{

    @SerializedName("iddispositivos")
    @Expose
    private int iddispositivos;
    @SerializedName("token")
    @Expose
    private String token;
    private final static long serialVersionUID = 4632343853830883339L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Dispositivo() {
    }

    /**
     * 
     * @param iddispositivos id del dispositivo
     * @param token Token firebase del dispositivo
     */
    public Dispositivo(int iddispositivos, String token) {
        super();
        this.iddispositivos = iddispositivos;
        this.token = token;
    }

    public int getIddispositivos() {
        return iddispositivos;
    }

    public void setIddispositivos(int iddispositivos) {
        this.iddispositivos = iddispositivos;
    }

    public Dispositivo withIddispositivos(int iddispositivos) {
        this.iddispositivos = iddispositivos;
        return this;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Dispositivo withToken(String token) {
        this.token = token;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("iddispositivos", iddispositivos).append("token", token).toString();
    }

}
