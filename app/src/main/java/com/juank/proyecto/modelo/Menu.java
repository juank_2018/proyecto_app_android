
package com.juank.proyecto.modelo;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Clase para representar un objeto Menu.
 */
public class Menu implements Serializable
{

    @SerializedName("idmenu")
    @Expose
    private int idmenu;
    @SerializedName("fecha")
    @Expose
    private int fecha;
    private final static long serialVersionUID = -3253680157838781586L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Menu() {
    }

    /**
     * 
     * @param idmenu id del menú
     * @param fecha fecha del menu
     */
    public Menu(int idmenu, int fecha) {
        super();
        this.idmenu = idmenu;
        this.fecha = fecha;
    }

    public int getIdmenu() {
        return idmenu;
    }

    public void setIdmenu(int idmenu) {
        this.idmenu = idmenu;
    }

    public Menu withIdmenu(int idmenu) {
        this.idmenu = idmenu;
        return this;
    }

    public int getFecha() {
        return fecha;
    }

    public void setFecha(int fecha) {
        this.fecha = fecha;
    }

    public Menu withFecha(int fecha) {
        this.fecha = fecha;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("idmenu", idmenu).append("fecha", fecha).toString();
    }

}
