
package com.juank.proyecto.modelo;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Clase para representar un usuario
 */
public class Usuario implements Serializable
{

    @SerializedName("idusuarios")
    @Expose
    private int idusuarios;
    @SerializedName("nombre")
    @Expose
    private String nombre;
    @SerializedName("apellido1")
    @Expose
    private String apellido1;
    @SerializedName("apellido2")
    @Expose
    private String apellido2;
    @SerializedName("fechanacimiento")
    @Expose
    private String fechanacimiento;
    private final static long serialVersionUID = -8481391218920361797L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Usuario() {
    }

    /**
     * 
     * @param apellido2
     * @param nombre
     * @param idusuarios
     * @param apellido1
     * @param fechanacimiento
     */
    public Usuario(int idusuarios, String nombre, String apellido1, String apellido2, String fechanacimiento) {
        super();
        this.idusuarios = idusuarios;
        this.nombre = nombre;
        this.apellido1 = apellido1;
        this.apellido2 = apellido2;
        this.fechanacimiento = fechanacimiento;
    }

    public int getIdusuarios() {
        return idusuarios;
    }

    public void setIdusuarios(int idusuarios) {
        this.idusuarios = idusuarios;
    }

    public Usuario withIdusuarios(int idusuarios) {
        this.idusuarios = idusuarios;
        return this;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Usuario withNombre(String nombre) {
        this.nombre = nombre;
        return this;
    }

    public String getApellido1() {
        return apellido1;
    }

    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    public Usuario withApellido1(String apellido1) {
        this.apellido1 = apellido1;
        return this;
    }

    public String getApellido2() {
        return apellido2;
    }

    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

    public Usuario withApellido2(String apellido2) {
        this.apellido2 = apellido2;
        return this;
    }

    public String getFechanacimiento() {
        return fechanacimiento;
    }

    public void setFechanacimiento(String fechanacimiento) {
        this.fechanacimiento = fechanacimiento;
    }

    public Usuario withFechanacimiento(String fechanacimiento) {
        this.fechanacimiento = fechanacimiento;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("idusuarios", idusuarios).append("nombre", nombre).append("apellido1", apellido1).append("apellido2", apellido2).append("fechanacimiento", fechanacimiento).toString();
    }

}
