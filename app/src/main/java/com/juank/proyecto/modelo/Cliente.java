package com.juank.proyecto.modelo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.List;

import javax.validation.Valid;

/**
 * Clase para representar un objeto cliente.
 */
public class Cliente implements Serializable {

    private final static long serialVersionUID = -7358381036408432770L;
    @SerializedName("idclientes")
    @Expose
    private int idclientes;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("paswd")
    @Expose
    private String paswd;
    @SerializedName("dispositivos")
    @Expose
    @Valid
    private List<Dispositivo> dispositivos = null;
    @SerializedName("usuario")
    @Expose
    @Valid
    private Usuario usuario;
    private Cliente cliente;

    /**
     * No args constructor for use in serialization
     */
    public Cliente() {
    }

    /**
     * @param dispositivos lista con los dispositivos del cliente
     * @param usuario      Objeto usuario con los datos de usuario del cliente
     * @param email        correo electrónico del cliente
     * @param idclientes   id del cliente
     * @param paswd        contraseña
     */
    public Cliente(int idclientes, String email, String paswd, List<Dispositivo> dispositivos, Usuario usuario) {
        super();
        this.idclientes = idclientes;
        this.email = email;
        this.paswd = paswd;
        this.dispositivos = dispositivos;
        this.usuario = usuario;
    }

    public int getIdclientes() {
        return idclientes;
    }

    public void setIdclientes(int idclientes) {
        this.idclientes = idclientes;
    }

    public Cliente withIdclientes(int idclientes) {
        this.idclientes = idclientes;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Cliente withEmail(String email) {
        this.email = email;
        return this;
    }

    public String getPaswd() {
        return paswd;
    }

    public void setPaswd(String paswd) {
        this.paswd = paswd;
    }

    public Cliente withPaswd(String paswd) {
        this.paswd = paswd;
        return this;
    }

    public List<Dispositivo> getDispositivos() {
        return dispositivos;
    }

    public void setDispositivos(List<Dispositivo> dispositivos) {
        this.dispositivos = dispositivos;
    }

    public Cliente withDispositivos(List<Dispositivo> dispositivos) {
        this.dispositivos = dispositivos;
        return this;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Cliente withUsuario(Usuario usuario) {
        this.usuario = usuario;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("idclientes", idclientes).append("email", email).append("paswd", paswd).append("dispositivos", dispositivos).append("usuario", usuario).toString();
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
}
