package com.juank.proyecto.controlador;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.juank.proyecto.R;
import com.juank.proyecto.Utilidades.Constantes;
import com.juank.proyecto.modelo.SeccionCarta;
import com.juank.proyecto.servicios.RestApiAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
    Fragment para mostrar el contenido de una carta con tabs

 */
public class TabsFragment extends Fragment {

    private static final String TAG = "TabsFragment";
    private static final String ARG_PARAM1 = "param1";

    @BindView(R.id.viewPager)
    ViewPager mViewPager;
    @BindView(R.id.tabLayout)
    TabLayout mTabLayout;
    Unbinder unbinder;

    private ProgressDialog progressDoalog;

    private List<SeccionCarta> lista;

    private int midCarta;
    private TabsPager mTabsPager;
    List<Fragment> mFragmentList = new ArrayList<>();
    List<String> mTitleList = new ArrayList<>();
    private String mIp;
    private OnFragmentInteractionListener mListener;

    public TabsFragment() {
        // Required empty public constructor
    }

    /**
        Crea una instancia del fragment con argumentos
     *
     * @param idCarta el id de la carta.
     * @return A new instance of fragment TabsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TabsFragment newInstance(int idCarta) {
        TabsFragment fragment = new TabsFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, idCarta);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            midCarta = getArguments().getInt(ARG_PARAM1);

        }
        cargaDatos(midCarta);
    }

    @Override
    public void onResume() {
        super.onResume();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        mIp = prefs.getString("ip", "");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_tabs, container, false);

        unbinder = ButterKnife.bind(this, v);

        mTabsPager = new TabsPager(getFragmentManager());

        mTabsPager.setFragmentList(mFragmentList);
        mTabsPager.setTitleList(mTitleList);
        Log.i(TAG, "onCreateView: " + mTabsPager.getFragmentList().size() + " " + mTabsPager.getTitleList().size());
        mViewPager.setAdapter(mTabsPager);
        mTabLayout.setupWithViewPager(mViewPager);

        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public void cargaDatos(int idCarta) {
        /*obtiene ip*/
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        mIp=prefs.getString("ip","");
        /*Prepara llamada*/
        Call<List<SeccionCarta>> call = RestApiAdapter.getApiService(mIp).listaPlatosCarta(idCarta);

        progressDoalog = new ProgressDialog(getContext());
        progressDoalog.setMax(100);
        progressDoalog.setTitle(getString(R.string.progress_cargando));
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        // muestro el dialog
        progressDoalog.show();
        call.enqueue(new Callback<List<SeccionCarta>>() {
            @Override
            public void onResponse(Call<List<SeccionCarta>> call, Response<List<SeccionCarta>> response) {

                Log.i(TAG, "onResponse: ");
                progressDoalog.dismiss();
                int responseCode = response.code();
                if (response.isSuccessful()) {

                    switch (responseCode) {
                        case Constantes.OK_200: {
                            lista = response.body();
                            Log.i(TAG, "onResponse: succesfull, call updateui");

                            for (SeccionCarta sc : lista) {
                                mTitleList.add(sc.getTipoPlato());
                                CartaDetalleFragment fragment = CartaDetalleFragment.newInstance(sc.getPlatosSeccion());
                                mFragmentList.add(fragment);

                            }
                            Log.i(TAG, "onResponse: " + mFragmentList.size() + "" + mTitleList.size());
                            mTabsPager.notifyDataSetChanged();
                        }
                        break;

                    }

                }else if(responseCode == Constantes.NOT_FOUND_404){
                    Toast.makeText(getContext(), R.string.error_no_existe_carta, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<SeccionCarta>> call, Throwable t) {
                progressDoalog.dismiss();
                Log.i(TAG, "onFailure: " + t.getLocalizedMessage());
            }
        });

    }

    public class TabsPager extends FragmentStatePagerAdapter {
        private List<Fragment> mFragmentList = new ArrayList<>();
        private List<String> mTitleList = new ArrayList<>();

        public List<Fragment> getFragmentList() {
            return mFragmentList;
        }

        public void setFragmentList(List<Fragment> fragmentList) {
            mFragmentList = fragmentList;
        }

        public List<String> getTitleList() {
            return mTitleList;
        }

        public void setTitleList(List<String> titleList) {
            mTitleList = titleList;
        }

        public TabsPager(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Log.i(TAG, "getItem: " +mFragmentList.get(position));
            Log.i(TAG, "getItem: " + mFragmentList.get(position).getArguments().getParcelableArrayList("platos").toString());
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            Log.i(TAG, "getCount: "+mFragmentList.size());
            return mFragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Log.i(TAG, "getPageTitle: "+mTitleList.get(position));
            return mTitleList.get(position);
        }

        @Override
        public int getItemPosition(Object object) {
            Log.i(TAG, "getItemPosition: ");
                    
                    
            return POSITION_NONE;
        }
    }
}
