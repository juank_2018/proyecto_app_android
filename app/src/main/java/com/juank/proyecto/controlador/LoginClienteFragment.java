package com.juank.proyecto.controlador;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.juank.proyecto.R;
import com.juank.proyecto.Utilidades.Constantes;
import com.juank.proyecto.modelo.Cliente;
import com.juank.proyecto.modelo.Empleado;
import com.juank.proyecto.servicios.RestApiAdapter;

import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 * Fragment para la gestión del login del cliente
 */
public final class LoginClienteFragment extends Fragment {
    private static final String TAG = "LoginClienteFragment";

    /*Constante para almacenar el nombre de usuario y passwd en  sharedpreferences*/
    private static final String USER_PASS_PREFS = "user_pass_prefs";
    /*Constante para almacenar en las preferencias el token del dispositivo*/
    private static final String TOKEN_DISPOSITIVO = "token_dispositivo";
    /*Resultado que debe recibir de la actividad de registro si ha sido correcto*/
    private static final int REQUEST_CODE_REGISTRADO = 0;


    Unbinder unbinder;
    @BindView(R.id.etEmail)
    EditText etEmail;
    @BindView(R.id.etPasswd)
    EditText etPasswd;
    @BindView(R.id.bEntrar)
    Button mBEntrar;
    @BindView(R.id.tvContrasena)
    TextView mTvContrasena;
    @BindView(R.id.tvRegistro)
    TextView mTvRegistro;
    @BindView(R.id.imageButton)
    ImageButton mImageButton;
    @BindView(R.id.fragment_login)
    ConstraintLayout mFragmentLogin;
    Unbinder unbinder1;
     String token;
     String mIp;
     ProgressDialog progressDoalog;


    public LoginClienteFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FirebaseApp.initializeApp(getContext());
        /*Cargo los valores por defecto en las preferencias*/
        PreferenceManager.setDefaultValues(getContext(), R.xml.preferencias, false);
        /*Según se abra la app, obtenemos el token y se almacena en prefs*/
        /*Obtengo el token de la aplicación*/
        token = FirebaseInstanceId.getInstance().getToken();
        Log.i(TAG, "onCreate: token= " + token);
        /*Primero compruebo si ya hay un token en las preferencias */
        SharedPreferences prefs = getActivity().getSharedPreferences(TOKEN_DISPOSITIVO, MODE_PRIVATE);
        String restoredText = prefs.getString("token", null);
        Log.i(TAG, "onCreate: restoredText: " + restoredText);
        if (restoredText != null) {/*si hay un token lo comparo por si ha cambiado y si ha cambiado lo sustituyo*/
            String token_almacenado = prefs.getString("token", "");
            if (!token.equals(token_almacenado)) {
                prefs.edit().putString("token", token).apply();
            }
        } else {/*si no hay un token lo almaceno*/
            prefs.edit().putString("token", token).apply();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        mIp = prefs.getString("ip", "");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login_cliente, container, false);
        unbinder1 = ButterKnife.bind(this, view);
        /*al abrir comprueba si están los datos almacenado y si están rellena los campos email y password*/
        SharedPreferences prefs = this.getActivity().getSharedPreferences(USER_PASS_PREFS, MODE_PRIVATE);
        String restoredText = prefs.getString("username", null);
        if (restoredText != null) {
            String name = prefs.getString("username", "");
            String password = prefs.getString("password", "");
            etEmail.setText(name);
            etPasswd.setText(password);

        }


        return view;
    }

    /**
     *  Método que responde a la pulsación del botón de configuración
     *  Carga el fragmento SettingsFragment
      */
    @OnClick(R.id.imageButton)
    public void config() {
        getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment,new SettingsFragment(),null)
                    .addToBackStack(null)
                    .commit();

    }

    /**
     * Método que responde a la pulsación del botón entrar
     *
     */
    @OnClick(R.id.bEntrar)
    public void onClick() {
        /*Obtiene el email y la contraseña que ha introducido el usuario*/
        String mail = etEmail.getText().toString();
        String contrasena = etPasswd.getText().toString();
        Log.i(TAG, "onClick: entra");
        /*Valida los campos*/
        if (validaCampos(mail, contrasena)) {
            Log.i(TAG, "onClick: llama al rest");
            Log.i(TAG, "onClick: mail= " + mail);
            Log.i(TAG, "onClick: passwd= " + contrasena);
            /*Obtiene la ip/dominio del servidor*/
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
            mIp = prefs.getString("ip", "");
            Log.i(TAG, "onClick: " + mIp);
            if(token == null){
                token = FirebaseInstanceId.getInstance().getToken();
            }
            /*Prepara la llamada al REST*/
            Call<Cliente> call = RestApiAdapter.getApiService(mIp).getCliente(mail, contrasena,token);
            /*Inicia el progresDialog*/
            progressDoalog = new ProgressDialog(getContext());
            progressDoalog.setMax(100);
            progressDoalog.setTitle(getString(R.string.progress_cargando));
            progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDoalog.show();
            /*Encola la llamada al REST para que se haga en segundo plano*/
            call.enqueue(new ClienteCallBack());

        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder1.unbind();

    }

    /**
     * Método que responde a la pulsación sobre Registrar
     * Lanza la actividad RegistroActivity
     */
    @OnClick(R.id.tvRegistro)
    public void onClickRegistro() {
        Intent i = new Intent();
        i.setClass(getContext().getApplicationContext(), RegistroActivity.class);

        startActivityForResult(i, REQUEST_CODE_REGISTRADO);
    }

    /**
     * Método que responde a la pulsación sobre Recordar contraseña
     * Lanza la actividad RecordarContrasena
     */
    @OnClick(R.id.tvContrasena)
    public void onClickContrasena() {
        Intent i = new Intent();
        i.setClass(getContext().getApplicationContext(), RecordarContrasena.class);

        startActivity(i);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            return;
        }
        if (requestCode == REQUEST_CODE_REGISTRADO) {
            String email = data.getStringExtra(RegistroActivityFragment.EXTRA_REGISTRO_EMAIL);
            String passwd = data.getStringExtra(RegistroActivityFragment.EXTRA_REGISTRO_PASSWD);
            etEmail.setText(email);
            etPasswd.setText(passwd);

        }
    }

    public boolean validaEmail(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }

     boolean validaCampos(String mail, String passwd) {

        boolean resultado = true;
        if (mail.trim().equals("") || passwd.trim().equals("")) {
            etPasswd.setError("Debes introducir una contraseña");
            etEmail.setError("Debes introducir un e-mail");
            resultado = false;
        } else {

            if (!validaEmail(mail)) {
                etEmail.setError("Formato de e-mail incorrecto");
                resultado = false;
            }
            if (passwd.length() > 8) {
                etPasswd.setError("Máximo 8 caracteres.");
                resultado = false;
            }

        }
        return resultado;
    }

    /**
     * Clase para implementar la llamada de retorno que sucede cuando se obtiene respuesta del servicio REST
     */
    class ClienteCallBack implements Callback<Cliente> {


        /**
         * Método que arranca cuando se recibe una respuesta desde el servicio REST
         *
         * @param call
         * @param response
         */
        @Override
        public void onResponse(Call<Cliente> call, Response<Cliente> response) {
            /*Variable Cliente para recibir los datos del serivicio*/
            Cliente cliente;

            /*El servicio puede dar 3 respuestas, 200 -> ok, 401-> no autorizado, 404-> no encontrado
             * en esta variable cargo el código que llega en la respuesta para luego procesarlo*/
            int statusCode;
            progressDoalog.dismiss();
            statusCode = response.code();
            if (response.isSuccessful()) {
                Log.i(TAG, "onResponse: hay respuesta.");

                switch (statusCode) {

                    case Constantes.OK_200: {
                        /*Cargo los datos que se reciben en el cuerpo de la respuesta en un objeto
                         * de tipo Cliente*/
                        cliente = response.body();
                        /*almaceno el usuario ,la contraseña y el id en las preferencias para que no haya que volver a meterlos*/
                        SharedPreferences.Editor editor = getActivity().getSharedPreferences(USER_PASS_PREFS, MODE_PRIVATE).edit();
                        editor.putString("username", etEmail.getText().toString());
                        editor.putString("password", etPasswd.getText().toString());
                        editor.putInt("id", cliente.getIdclientes());
                        editor.apply();

                        Log.i(TAG, "onResponse: cliente= " + cliente.toString());
                        /*Inicia la actividad Principal y finaliza la de login.*/
                        Intent i = new Intent();
                        i.setClass(getContext().getApplicationContext(), Principal.class);
                        getActivity().finish();
                        startActivity(i);
                    }
                }


            } else {
                switch (statusCode) {
                    case Constantes.NOT_FOUND_404: {
                        /*El correo facilitado no existe en la bbdd*/
                        etEmail.setError("email sin registrar.");
                    }
                    break;
                    case Constantes.NOT_AUTHORIZED_401: {
                        /*La contraseña facilitada es incorrecta*/
                        etPasswd.setError("Contraseña incorrecta");
                    }
                    break;
                }
            }
        }

        @Override
        public void onFailure(Call<Cliente> call, Throwable t) {
            progressDoalog.dismiss();
            Toast.makeText(getContext(), R.string.error_problema_conexion, Toast.LENGTH_LONG).show();
        }
    }
}
