package com.juank.proyecto.controlador;


import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.juank.proyecto.R;
import com.juank.proyecto.Utilidades.Constantes;

import com.juank.proyecto.modelo.MenuREST;
import com.juank.proyecto.servicios.RestApiAdapter;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;



/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MenuFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MenuFragment extends Fragment {

private static final String TAG= "MenuFragment";
    @BindView(R.id.menu_recycler_view)
    RecyclerView mMenuRecyclerView;
    Unbinder unbinder;
    private String mFechaActual;
    private ProgressDialog progressDoalog;
    private List<MenuREST> mMenus;
    private MenuAdapter mAdapter;
    private int mCurrentPosition;
    private String mIp;

    public MenuFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment MenuFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MenuFragment newInstance() {
        MenuFragment fragment = new MenuFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        mFechaActual = sdf.format(new Date());


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_menu, container, false);
        unbinder = ButterKnife.bind(this, v);
        mMenuRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mMenuRecyclerView.setAdapter(mAdapter);
        cargarDatos();
        return v;
    }
    @Override
    public void onResume() {
        super.onResume();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        mIp=prefs.getString("ip","");
    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void cargarDatos(){
        /*obtengo la ip*/
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        mIp=prefs.getString("ip","");
        Log.i(TAG, "cargarDatos: "+mIp);
        /*Prepara la llamada al REST*/
        Call<List<MenuREST>> call = RestApiAdapter.getApiService(mIp).listaPlatosMenu(mFechaActual);
        // Preparo un progressDialog para que se muestre mientras se hace la llamada al REST

        progressDoalog = new ProgressDialog(getContext());
        progressDoalog.setMax(100);
        progressDoalog.setTitle(getString(R.string.progress_cargando));
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        // muestro el dialog
        progressDoalog.show();
        /*encolo la llamada*/
        call.enqueue(new Callback<List<MenuREST>>() {
            @Override
            public void onResponse(Call<List<MenuREST>> call, Response<List<MenuREST>> response) {
                /*si hay respuesta oculto el progressDialog*/
                progressDoalog.dismiss();
                int responseCode = response.code();
                /* Si la respuesta es satisfactoria ontengo el cuerpo y actualizo el recuclerview*/
                if(response.isSuccessful()){
                    switch (responseCode){
                        case Constantes.OK_200:{
                            mMenus=response.body();
                            updateUI();
                        }
                        break;

                    }
                /*si no ha obtenido nada, informo que el menu del dia no ha salido aún*/
                }else if (responseCode == Constantes.NOT_FOUND_404){

                        Toast.makeText(getContext(),R.string.error_no_hay_menu,Toast.LENGTH_LONG).show();


                }
            }

            @Override
            public void onFailure(Call<List<MenuREST>> call, Throwable t) {
                Toast.makeText(getContext(),R.string.error_problema_conexion, Toast.LENGTH_LONG).show();
            }
        });
    }

    /**
     * Método para actualizar el reciclerView cuando haya cambios en los datos
     */
    public void updateUI() {
        if (mAdapter == null) {
            mAdapter = new MenuAdapter(mMenus);
            mMenuRecyclerView.setAdapter(mAdapter);
        } else {
            if (mMenus.size() > 0) {
                mAdapter.notifyDataSetChanged();
            }
            mAdapter.setMenus(mMenus);
        }
    }

    /**
     * Clase para el adaptador del RecyclerView
     */
    protected class MenuAdapter extends RecyclerView.Adapter<MenuHolder> {
        private List<MenuREST> mMenus;

        @BindView(R.id.tvTitulo)
        TextView mTvTitulo;
        @BindView(R.id.tvPrimero)
        TextView mTvPrimero;
        @BindView(R.id.tvSegundo)
        TextView mTvSegundo;
        @BindView(R.id.tvTercero)
        TextView mTvTercero;

        public MenuAdapter(List<MenuREST> menus) {
            mMenus = menus;
        }

        @Override
        public MenuHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            View v = layoutInflater.inflate(R.layout.list_item_menu, parent, false);
            ButterKnife.bind(this,v);
            return new MenuHolder(v);
        }

        @Override
        public void onBindViewHolder(MenuHolder holder, int position) {
            MenuREST menu = mMenus.get(position);
            holder.bindMenu(menu);
        }

        @Override
        public int getItemCount() {
            return mMenus.size();
        }
        public void setMenus(List<MenuREST> menus) {
            mMenus = menus;
        }
    }

    protected class MenuHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvTitulo)
        TextView mTvTitulo;
        @BindView(R.id.tvPrimero)
        TextView mTvPrimero;
        @BindView(R.id.tvSegundo)
        TextView mTvSegundo;
        @BindView(R.id.tvTercero)
        TextView mTvTercero;

        private MenuREST mMenu;


        public MenuHolder(View itemView) {


            super(itemView);
            ButterKnife.bind(this,itemView);
        }
          public void bindMenu(MenuREST menu){
             mMenu=menu;
             mTvTitulo.setText(menu.getTipo());
             mTvPrimero.setText(menu.getPlatos().get(0));
              mTvSegundo.setText(menu.getPlatos().get(1));
              mTvTercero.setText(menu.getPlatos().get(2));
          }
    }
}
