package com.juank.proyecto.controlador;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.juank.proyecto.R;
import com.juank.proyecto.Utilidades.Constantes;
import com.juank.proyecto.modelo.Reserva;
import com.juank.proyecto.servicios.RestApiAdapter;

import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Fragment para listar las reservas.
 */
public class ReservasFragment extends Fragment {
    private static final String USER_MAIL = "username";
    private static final String TAG = "ReservasFragment";

    @BindView(R.id.reservas_recycler_view)
    RecyclerView mReservasRecyclerView;
    Unbinder unbinder;

    @BindView(R.id.floatingActionButton)
    FloatingActionButton mFloatingActionButton;

    private String mEmail;

    private List<Reserva> mListaReservas;
    private ReservaAdapter mAdapter;
    ProgressDialog progressDoalog;

    private int posicionActual = -1;

    private OnFragmentInteractionListener mListener;
    private String mIp;

    public ReservasFragment() {
        // Required empty public constructor
    }

    /**
     * Crea el fragment y le añade argumentos
     * @return A new instance of fragment ReservasFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ReservasFragment newInstance(String email) {
        ReservasFragment fragment = new ReservasFragment();
        Bundle args = new Bundle();
        args.putString(USER_MAIL, email);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mEmail = getArguments().getString(USER_MAIL);

        }
    }
    @Override
    public void onResume() {
        super.onResume();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        mIp=prefs.getString("ip","");
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_reservas, container, false);

        unbinder = ButterKnife.bind(this, v);
        Toolbar toolbar = getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.toolbar_reservas));

        mReservasRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mReservasRecyclerView.setAdapter(mAdapter);
        cargaDatos();
        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }
    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @OnClick(R.id.floatingActionButton)
    public void onClick(){
        mListener.onClickCrearReserva();
    }

    public interface OnFragmentInteractionListener{
        void onClickCrearReserva();
    }
    private void cargaDatos() {
        /*obtiene la ip*/
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        mIp=prefs.getString("ip","");

        /*prepara la llamada al REST*/
        Call<List<Reserva>> call = RestApiAdapter.getApiService(mIp).listaReservas(mEmail);
        /*muestra el progressdialog*/
        progressDoalog = new ProgressDialog(getContext());
        progressDoalog.setMax(100);
        progressDoalog.setTitle(getString(R.string.progress_cargando));
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        progressDoalog.show();
        /*encola la llamada*/
        call.enqueue(new Callback<List<Reserva>>() {
            @Override
            public void onResponse(Call<List<Reserva>> call, Response<List<Reserva>> response) {
                progressDoalog.dismiss();
                int responseCode=response.code();
                if (response.isSuccessful()) {

                    switch (responseCode){
                        case Constantes.OK_200:{
                            mListaReservas = response.body();
                            updateUI();
                        }
                        break;

                    }

                }else {
                    switch (responseCode){
                        case Constantes.NOT_FOUND_404:{
                            Toast.makeText(getContext(),R.string.error_no_hay_reservas,Toast.LENGTH_LONG).show();
                        }
                        break;
                    }
                }
            }

            @Override
            public void onFailure(Call<List<Reserva>> call, Throwable t) {
                progressDoalog.dismiss();
                Log.i(TAG, "onFailure: " + t.getLocalizedMessage());
                Toast.makeText(getContext(),R.string.error_problema_conexion,Toast.LENGTH_LONG).show();
            }
        });
    }

    public void updateUI() {


        if (mAdapter == null) {

            mAdapter = new ReservaAdapter(mListaReservas, getContext());
            mReservasRecyclerView.setAdapter(mAdapter);
        } else {
            if (mListaReservas.size() > 0) {

                mAdapter.notifyDataSetChanged();
            }
            mAdapter.setMlista(mListaReservas);
        }


    }

    protected class ReservaAdapter extends RecyclerView.Adapter<ReservaAdapter.ReservaHolder> {
        @BindView(R.id.list_item_reserva_fecha)
        TextView mListItemReservaFecha;
        @BindView(R.id.textView1)
        TextView mTextView1;
        @BindView(R.id.textView2)
        TextView mTextView2;
        @BindView(R.id.textView3)
        TextView mTextView3;
        @BindView(R.id.textViewComensales)
        TextView mTextViewComensales;
        @BindView(R.id.textViewTurno)
        TextView mTextViewTurno;
        @BindView(R.id.textViewHora)
        TextView mTextViewHora;
        @BindView(R.id.tablaReservas)
        TableLayout mTablaReservas;
        private List<Reserva> mlista;
        private Context mContext;

        public List<Reserva> getMlista() {
            return mlista;
        }

        public void setMlista(List<Reserva> mlista) {
            this.mlista = mlista;
        }

        public ReservaAdapter(List<Reserva> mlista, Context context) {
            this.mlista = mlista;
            mContext = context;
        }

        @Override
        public ReservaHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_reservas, parent, false);
            ButterKnife.bind(this, v);
            return new ReservaHolder(v);
        }

        @Override
        public void onBindViewHolder(ReservaHolder holder, final int position) {
            Reserva reserva = mListaReservas.get(position);
            String[] fechaHora = reserva.getFechaHora().split("-");


            holder.mListItemReservaFecha.setText(fechaHora[0]);
            holder.mTextViewComensales.setText(Integer.toString(reserva.getNComensales()));
            holder.mTextViewTurno.setText(reserva.getTurno());
            holder.mTextViewHora.setText(fechaHora[1]);
            holder.mTablaReservas.setVisibility(View.GONE);
            if (posicionActual == position) {
                Animation desliza = AnimationUtils.loadAnimation(mContext, R.anim.desliza);
                holder.mTablaReservas.setVisibility(View.VISIBLE);
                holder.mTablaReservas.startAnimation(desliza);
            }
            holder.mListItemReservaFecha.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    posicionActual = position;
                    notifyDataSetChanged();
                }
            });
        }

        @Override
        public int getItemCount() {
            return mlista.size();
        }

        class ReservaHolder extends RecyclerView.ViewHolder {
            @BindView(R.id.list_item_reserva_fecha)
            TextView mListItemReservaFecha;
            @BindView(R.id.textView1)
            TextView mTextView1;
            @BindView(R.id.textView2)
            TextView mTextView2;
            @BindView(R.id.textView3)
            TextView mTextView3;
            @BindView(R.id.textViewComensales)
            TextView mTextViewComensales;
            @BindView(R.id.textViewTurno)
            TextView mTextViewTurno;
            @BindView(R.id.textViewHora)
            TextView mTextViewHora;
            @BindView(R.id.tablaReservas)
            TableLayout mTablaReservas;

            public ReservaHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }
        }
    }
}
