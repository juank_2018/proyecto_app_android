package com.juank.proyecto.controlador;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.juank.proyecto.R;
import com.juank.proyecto.Utilidades.Constantes;
import com.juank.proyecto.modelo.SimpleResponse;
import com.juank.proyecto.servicios.RestApiAdapter;

import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Fragment para hacer el recordatorio de la contraseña
 */
public final class RecordarContrasenaFragment extends Fragment {
    private static final String TAG="RecordarContrasena";
    /*Binding de las vista de butterknife*/
    @BindView(R.id.etMailRecordar)
    protected EditText etMailRecordar;
    @BindView(R.id.bEnviarContrasena)
    protected Button bEnviarContrasena;
private String mIp;
    public RecordarContrasenaFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_recordar_contrasena, container, false);
        ButterKnife.bind(this,view);


        return view;
    }
    @Override
    public void onResume() {
        super.onResume();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        mIp=prefs.getString("ip","");
    }

    /**
     * Método que responde a la pulsación en el botón enviar contraseña
     */
    @OnClick(R.id.bEnviarContrasena)
    public void onClick(){
        /*Obtiene el email introducido en el cuadro de texto*/
        String mail= etMailRecordar.getText().toString();
        /*Obtiene la ip*/
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        mIp=prefs.getString("ip","");
        /*valida el campo*/
        if(validaCampos(mail)){
            /*Llama al servicio REST*/
            Call<SimpleResponse> call= RestApiAdapter.getApiService(mIp).enviaContrasena(mail);
            call.enqueue(new Callback<SimpleResponse>() {

                @Override
                public void onResponse(Call<SimpleResponse> call, Response<SimpleResponse> response) {
                    int statusCode = response.code();
                    Log.i(TAG, "onResponse: code= "+statusCode);
                    if(response.isSuccessful()){
                        switch (statusCode){
                            case Constantes.OK_200:{
                                Log.i(TAG, "onResponse: 200");
                                Toast.makeText(getActivity().getApplicationContext(),R.string.envioContrasena,Toast.LENGTH_SHORT).show();
                            }
                            break;
                        }
                    }else {
                        switch (statusCode){
                            case Constantes.NOT_FOUND_404:{
                                Log.i(TAG, "onResponse: 404");
                                Toast.makeText(getActivity().getApplicationContext(),R.string.emailNoExite,Toast.LENGTH_SHORT).show();
                            }
                            break;

                            case Constantes.SERVER_ERROR_500:{
                                Log.i(TAG, "onResponse: 500");
                                Toast.makeText(getActivity().getApplicationContext(),R.string.errorEnvioContrasena,Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                }

                @Override
                public void onFailure(Call<SimpleResponse> call, Throwable t) {

                    Log.i(TAG, "onFailure: "+t.getLocalizedMessage());
                    //Log.i(TAG,);
                    Toast.makeText(getActivity().getApplicationContext(),R.string.errorEnvioContrasena,Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    /**
     * Valida si una cadena es un mail válido
     * @param email String con el email
     * @return true correcto, false incorrecto
     */
    public boolean validaEmail(String email){
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }

    /**
     * Método para validar campos
     * @param mail
     * @return
     */
    private boolean validaCampos(String mail){

        boolean resultado = true;
        if(mail.trim().equals("") ){

            etMailRecordar.setError("Debes introducir un e-mail");
            resultado= false;
        }else{

            if (!validaEmail(mail)){
                etMailRecordar.setError("Formato de e-mail incorrecto");
                resultado= false;
            }

        }
        return resultado;
    }
}
