package com.juank.proyecto.controlador;


import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;

import com.google.firebase.FirebaseApp;
import com.google.firebase.messaging.FirebaseMessaging;
import com.juank.proyecto.R;
import com.juank.proyecto.modelo.Carta;

import butterknife.BindView;
import butterknife.ButterKnife;


public class Principal extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, TabsFragment.OnFragmentInteractionListener, ReservasFragment.OnFragmentInteractionListener, CartaFragment.OnFragmentInteractionListener {
    private static final String TAG = "Principal";
    /*Constante para almacenar el nombre de usuario y passwd en  sharedpreferences*/
    private static final String USER_PASS_PREFS = "user_pass_prefs";
    /* primera vez que se ejecuta la app*/
    private static final String FIRST_RUN = "first_run";
    /*Firebase topics*/
    private static final String MENU = "menu";
    private static final String TEMPORADA = "temporada";
    private static final String PROMOCIONES = "promociones";
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;
    @BindView(R.id.appbar)
    AppBarLayout mAppbar;
    private String mEmail;
    private SharedPreferences settings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FirebaseApp.initializeApp(this);
        setContentView(R.layout.activity_principal);
        ButterKnife.bind(this);

        settings = PreferenceManager.getDefaultSharedPreferences(this);
        PreferenceManager.setDefaultValues(this, R.xml.preferencias, false);


        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawerLayout, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.addDrawerListener(toggle);
        toggle.syncState();


        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        /*si es la primera vez que se lanza la aplicación la subscribo a todos los topics para
         * que reciba todos los tipos de notificaciones*/
        if (settings.getBoolean(FIRST_RUN, true)) {
            FirebaseMessaging.getInstance().subscribeToTopic(MENU);
            FirebaseMessaging.getInstance().subscribeToTopic(TEMPORADA);
            FirebaseMessaging.getInstance().subscribeToTopic(PROMOCIONES);
            /*ahora pongo la preferencia first_run a false */
            settings.edit().putBoolean(FIRST_RUN, false).apply();
        }
    }

    @Override
    public void onBackPressed() {

        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.principal, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_carta) {
            /* Creo un nuevo CartaFragment y lo coloco en el contenedor*/
            FragmentManager manager = getSupportFragmentManager();
            CartaFragment cartaFragment = CartaFragment.newInstance();
            manager.beginTransaction().replace(R.id.contenedor, cartaFragment).addToBackStack(null).commit();

        } else if (id == R.id.nav_menu) {
            FragmentManager manager = getSupportFragmentManager();
            MenuFragment menuFragment = MenuFragment.newInstance();
            manager.beginTransaction().replace(R.id.contenedor, menuFragment).addToBackStack(null).commit();

        } else if (id == R.id.nav_reservas) {
            /*Obtengo el email del cliente de las preferencias y se lo paso al ReservasFragment como argumento*/
            SharedPreferences prefs = getSharedPreferences(USER_PASS_PREFS, MODE_PRIVATE);
            mEmail = prefs.getString("username", "");
            FragmentManager manager = getSupportFragmentManager();
            ReservasFragment reservasFragment = ReservasFragment.newInstance(mEmail);
            manager.beginTransaction().replace(R.id.contenedor, reservasFragment).addToBackStack(null).commit();
        } else if (id == R.id.nav_notificaciones) {
            /*Obtengo el email del cliente de las preferencias y se lo paso a NotificacionesFragment como argumento*/
            SharedPreferences prefs = getSharedPreferences(USER_PASS_PREFS, MODE_PRIVATE);
            mEmail = prefs.getString("username", "");
            FragmentManager manager = getSupportFragmentManager();
            NotificacionesFragment notificacionesFragment = NotificacionesFragment.newInstance(mEmail);
            manager.beginTransaction().replace(R.id.contenedor, notificacionesFragment).addToBackStack(null).commit();

        } else if (id == R.id.nav_config) {

            FragmentManager manager = getSupportFragmentManager();
            SettingsFragment settings = new SettingsFragment();
            manager.beginTransaction().replace(R.id.contenedor, settings).addToBackStack(null).commit();


        } else if (id == R.id.nav_salir) {
            /*si pulsa desconectar termino la activity en la que esta´y crgo la activity de login*/
            Intent i = new Intent();
            i.setClass(getApplicationContext(), LoginCliente.class);
            finish();
            startActivity(i);
        }


        mDrawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    /**
     * Método que responde cuando el usuario toca en una carta en el listado de cartas
     * carga un TabsFragment para cargar el detalle de la carta.
     *
     * @param carta
     */
    @Override
    public void onCartaSelected(Carta carta) {
        FragmentManager manager = getSupportFragmentManager();
        TabsFragment tabFragment = TabsFragment.newInstance(carta.getIdcarta());
        manager.beginTransaction().replace(R.id.contenedor, tabFragment).addToBackStack(null).commit();

    }

    /**
     * Método que responde a la pulsación del floatButton del fragment de reservas.
     * carga el Fragment para solicitar reservas.
     */
    @Override
    public void onClickCrearReserva() {
        FragmentManager manager = getSupportFragmentManager();
        CrearReservaFragment crearReservaFragment = CrearReservaFragment.newInstance();
        manager.beginTransaction().replace(R.id.contenedor, crearReservaFragment).addToBackStack(null).commit();
    }

    /**
     * Si el usuario pulsa la tecla atrás muestra un cuadro de dialogo preguntando si quiere salir de la aplicaión
     *
     * @param keyCode
     * @param event
     * @return
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK) {

            new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("Salir")
                    .setMessage("Estás seguro?")
                    .setNegativeButton(android.R.string.cancel, null)// sin listener
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {// un listener que al pulsar, cierre la aplicacion
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
// Salir
                            Principal.this.finish();
                        }
                    })
                    .show();

// Si el listener devuelve true, significa que el evento esta procesado, y nadie debe hacer nada mas
            return true;
        }
// para las demas cosas, se reenvia el evento al listener habitual
        return super.onKeyDown(keyCode, event);
    }
}
