package com.juank.proyecto.controlador;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TableLayout;
import android.widget.TextView;

import com.juank.proyecto.R;
import com.juank.proyecto.modelo.Plato;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Fragmento para mostrar los platos que componen una carta
 * se compone de un RecyclerView para mostrar la lista de platos..
 */
public class CartaDetalleFragment extends Fragment {
    private static final String TAG = "CartaDetalleFragment";

    private static final String LISTA_PLATOS = "platos";

    /*RecycleView para mostrar los platos*/
    @BindView(R.id.carta_detalle_recycler_view)
    RecyclerView mCartaDetalleRecyclerView;
    /*unbinder de ButterKinfe para inyectar vistas*/
    Unbinder unbinder;

    /*Adaptador para el RecyclerView*/
    private DetallCartaAdapter mAdapter;
    /*posición del elemento en el recyclerView*/
    /*Se inicializa a -1 para que el primer elemento del RecyclerView no tenga desplegado la tabla
     * de precios de un principio*/
    private int posicionActual = -1;
    /*lista de platos*/
    private ArrayList<Plato> mPlatos;


    public CartaDetalleFragment() {
        // Required empty public constructor
    }

    /**
     * Método factory que crea una instancia del la clase {@link CartaDetalleFragment} incluyendo
     * como parámetro la lista de platos.
     *
     * @param platos lista de platos.
     * @return A new instance of fragment CartaDetalleFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CartaDetalleFragment newInstance(ArrayList<Plato> platos) {

        CartaDetalleFragment fragment = new CartaDetalleFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(LISTA_PLATOS, platos);
        fragment.setArguments(args);
        Log.i(TAG, "newInstance: crea fragment");
        return fragment;
    }

    /**
     * Método del ciclo de vida del fragment. En la creación comprueba si hay argumentos y si los
     * hay los asigna a la variable mPlatos.
     *
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mPlatos = getArguments().getParcelableArrayList(LISTA_PLATOS);

        }

    }

    /**
     * Método que crea la vista del fragment
     *
     * @param inflater           objeto inflater para cargar la vista desde el xml
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Log.i(TAG, "onCreateView: ");
        View v = inflater.inflate(R.layout.fragment_carta_detalle, container, false);
        /*inyeción de las vistas con ButterKnife*/
        unbinder = ButterKnife.bind(this, v);
        /*Creación del RecyclerView*/
        mCartaDetalleRecyclerView.setHasFixedSize(true);
        mCartaDetalleRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        /*Se crea una instancia de DetallCartaAdapter y de asigna al RecyclerView*/
        mAdapter = new DetallCartaAdapter(mPlatos, getContext());
        mCartaDetalleRecyclerView.setAdapter(mAdapter);

        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        /*Al destruir la vista me aseguro que ButterKinfe libera la memoria*/
        unbinder.unbind();
    }

    /**
     * Clase para crear el adaptador que va a poblar el RecyclerView
     */
    protected class DetallCartaAdapter extends RecyclerView.Adapter<DetallCartaAdapter.CartaDetalleViewHolder> {

        /*Vistas inyectads con ButterKnife*/

        @BindView(R.id.list_item_carta_detalle_nombre)
        TextView mListItemCartaDetalleNombre;
        @BindView(R.id.textViewTapa)
        TextView mTextViewTapa;
        @BindView(R.id.textViewMedia)
        TextView mTextViewMedia;
        @BindView(R.id.textViewRacion)
        TextView mTextViewRacion;
        @BindView(R.id.textView6)
        TextView mTextView6;
        @BindView(R.id.textView5)
        TextView mTextView5;
        @BindView(R.id.textView4)
        TextView mTextView4;
        @BindView(R.id.tablaPrecios)
        TableLayout mTablaPrecios;
        /*Lista de platos*/
        private ArrayList<Plato> lista;
        private Context context;

        /**
         * Constructor del Adapter
         *
         * @param lista   Lista de platos
         * @param context Contexto
         */
        DetallCartaAdapter(ArrayList<Plato> lista, Context context) {
            this.lista = lista;
            this.context = context;
        }

        public void setLista(ArrayList<Plato> lista) {
            this.lista = lista;
        }


        @Override
        public CartaDetalleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            Log.i(TAG, "onCreateViewHolder: ");
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_carta_detalle, parent, false);
            ButterKnife.bind(this, v);
            return new CartaDetalleViewHolder(v);
        }

        @Override
        public void onBindViewHolder(final CartaDetalleViewHolder holder, int position) {
            /*Obtiene el plato */
            Plato plato = lista.get(position);
            Log.i(TAG, "onBindViewHolder: " + plato.toString());
            /*Rellena lo elementos de la vista con los datos del plato*/
            holder.mListItemCartaDetalleNombre.setText((CharSequence) plato.getNombre());
            holder.mTextView6.setText(plato.getPrecioTapa() + "€");
            holder.mTextView5.setText(plato.getPrecioMedia() + "€");
            holder.mTextView4.setText(plato.getPrecioRacion() + "€");
            /*En principo el TableLayout en el que están los precios está oculto*/
            holder.mTablaPrecios.setVisibility(View.GONE);
            /*Si posicionActual coincide con position, o sea con la posicion del adapter entonces
             * se despliega el TableLayout con la animación para que se vean los precios.*/
            if (posicionActual == position) {
                Animation desliza = AnimationUtils.loadAnimation(context, R.anim.desliza);
                holder.mTablaPrecios.setVisibility(View.VISIBLE);
                holder.mTablaPrecios.startAnimation(desliza);
            }
            /*Un listener sobre el textview del nombre para controlar cuando se pulsa */
            holder.mListItemCartaDetalleNombre.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    posicionActual = holder.getAdapterPosition();
                    notifyDataSetChanged();
                }
            });
        }

        @Override
        public int getItemCount() {
            return lista.size();
        }

        class CartaDetalleViewHolder extends RecyclerView.ViewHolder {
            @BindView(R.id.list_item_carta_detalle_nombre)
            TextView mListItemCartaDetalleNombre;

            @BindView(R.id.textViewTapa)
            TextView mTextViewTapa;
            @BindView(R.id.textViewMedia)
            TextView mTextViewMedia;
            @BindView(R.id.textViewRacion)
            TextView mTextViewRacion;
            @BindView(R.id.textView6)
            TextView mTextView6;
            @BindView(R.id.textView5)
            TextView mTextView5;
            @BindView(R.id.textView4)
            TextView mTextView4;
            @BindView(R.id.tablaPrecios)
            TableLayout mTablaPrecios;

            public CartaDetalleViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }

        }
    }
}
