package com.juank.proyecto.controlador;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;

import com.juank.proyecto.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by jcpm0 on 22/03/2018.
 */

public class DatePickerFragment extends DialogFragment {
    private static final String ARG_DATE = "date";
    private static final String ARG_LIMIT="limit";
    public static final String EXTRA_DATE = "com.juank.proyecto.controlador";
    private static final String TAG = "DAtepickerFrag";
    @BindView(R.id.dialog_date_date_picker)
    DatePicker mDialogDateDatePicker;
    Unbinder unbinder1;



    public static DatePickerFragment newInstance(Date date,boolean limit) {
        Bundle args = new Bundle();
        args.putSerializable(ARG_DATE, date);
        args.putBoolean(ARG_LIMIT,limit);
        DatePickerFragment fragment = new DatePickerFragment();
        fragment.setArguments(args);
        return fragment;

    }

    @Override
    public void onStart() {
        super.onStart();
        Log.i(TAG, "onStart: ");
unbinder1=ButterKnife.bind(this,getDialog());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder1.unbind();
    }



    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Date date = (Date) getArguments().getSerializable(ARG_DATE);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        boolean limite=getArguments().getBoolean(ARG_LIMIT);
        View v = LayoutInflater.from(getContext()).inflate(R.layout.dialog_date, null);

        ButterKnife.bind(this,v);
        // mDatePicker=(DatePicker)v.findViewById(R.id.dialog_date_date_picker);
        mDialogDateDatePicker.init(year, month, day, null);
        if(limite){
            /*limite inferior*/
            calendar.set(year,month,day-1);
            mDialogDateDatePicker.setMinDate(calendar.getTimeInMillis());
            /*limite superior*/
            calendar.set(year,month,day+7);
            mDialogDateDatePicker.setMaxDate(calendar.getTimeInMillis());
        }
        return new AlertDialog.Builder(getActivity()).setView(v)
                .setTitle(R.string.date_picker_title)
                .setPositiveButton(android.R.string.ok,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                int year = mDialogDateDatePicker.getYear();
                                int month = mDialogDateDatePicker.getMonth();
                                int day = mDialogDateDatePicker.getDayOfMonth();
                                Calendar fechaReserva = new GregorianCalendar();
                                fechaReserva.set(year,month,day);
                                SimpleDateFormat sdf= new SimpleDateFormat("dd/MM/yyyy");
                                String date = sdf.format(fechaReserva.getTime());
                                sendResult(Activity.RESULT_OK, date);
                            }
                        })
                .create();

    }

    private void sendResult(int resultCode, String date) {
        if (getTargetFragment() == null) {
            return;
        }
        Intent intent = new Intent();
        intent.putExtra(EXTRA_DATE, date);
        getTargetFragment().onActivityResult(getTargetRequestCode(), resultCode, intent);
    }




}
