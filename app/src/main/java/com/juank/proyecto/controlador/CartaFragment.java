package com.juank.proyecto.controlador;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.juank.proyecto.R;
import com.juank.proyecto.Utilidades.Constantes;
import com.juank.proyecto.modelo.Carta;
import com.juank.proyecto.servicios.RestApiAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Fragmnent para consultar las cartas que tiene disponibles el restaurante.
 * Se compone de un RecyclerView para listar las cartas.
 * Utiliza retrofit para llamar al servicio REST y obtener el listado de cartas.
 */
public class CartaFragment extends Fragment {
    private static final String TAG = "CartaFragment";
    /*variable para almacenar la ip del serivicio REST*/
    private String mIp;
    /*Posición actual en el Recyclerview*/
    private int mCurrentPosition;
    /*Adapter para el RecyclerView*/
    private CartaAdapter mAdapter;
    /*Barra de progreso */
    private ProgressDialog progressDoalog;
    /*Lista de cartas disponibles*/
    List<Carta> mCartas;
    /*unbider de ButterKinfe, necesario para poder inyectar las vistas */
    Unbinder unbinder;
    /*El RecyclerView*/
    @BindView(R.id.carta_recycler_view)
    RecyclerView mCartaRecyclerView;


    /*Listener para hacer el callback a la actividad principal cuando se pulse sobre el nombre de una carta*/
   private OnFragmentInteractionListener mListener;

    /**
     * Constructor vacio.
     */
    public CartaFragment() {
        // Required empty public constructor
    }

    /**
     * Métdo Factory que crea un nuevo fragment
     * @return
     */
    public static CartaFragment newInstance() {
        CartaFragment fragment = new CartaFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_carta, container, false);
        /*inyección de vistas con ButterKnife*/
        unbinder = ButterKnife.bind(this, view);
        /*Cambio el titulo de la toolbar*/
        Toolbar toolbar = getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.toolbar_carta));
        /*RecyclerView*/
        mCartaRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        mCartaRecyclerView.setAdapter(mAdapter);
        Log.i(TAG, "onCreateView: "+mCurrentPosition);
        /*Llamo al método para obtener los datos del servicio REST*/
        obtenerDatos();
        return view;
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
    @Override
    public void onResume() {
        super.onResume();
        /*Siempre que el fragmento pase a onresume me aseguro de volver a cargar la ip de las preferencias no sea que haya cambiado*/
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        mIp=prefs.getString("ip","");
    }
    /**
        Interface para el callback a la actividad principal
     */
    public interface OnFragmentInteractionListener {

        void onCartaSelected(Carta carta);
    }

    /**
     * Método para obtener los datos del REST
     */
    public void obtenerDatos(){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        mIp=prefs.getString("ip","");
        /*Instancio un objeto Call de retrofit con la llamada al método del servicio para obtener la lista de cartas*/
        Call<List<Carta>> call = RestApiAdapter.getApiService(mIp).listaCartas();

        // Preparo un progressDialog para que se muestre mientras se hace la llamada al REST
        progressDoalog = new ProgressDialog(getContext());
        progressDoalog.setMax(100);
        progressDoalog.setTitle(getString(R.string.progress_cargando));
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        // muestro el dialog
        progressDoalog.show();
        /*encolo el trabajo para que retrofit de encargue de hacerlo de forma asincrona*/
        call.enqueue(new CartaCallback());

        }
    /*clase que implementa el interface de Retrofit para el callback, aquí es donde nos devuelve el
    * resultado el hilo que ha iniciado */
    class CartaCallback implements Callback<List<Carta>>{

        @Override
        public void onResponse(Call<List<Carta>> call, Response<List<Carta>> response) {
            /*Si hay respuesta oculto del progressdialog*/
            progressDoalog.dismiss();
            /*Que la respuesta sea satisfactoria no quiere decir que sea la correcta, solo quiere
            * decir que no ha habido errores en la conexión pero hay que ver que código ha devuelto
            * y manejarlo*/
            if (response.isSuccessful()) {
                int responseCode = response.code();
                switch (responseCode){
                    case Constantes.OK_200:{
                        Log.i(TAG, "onResponse: " + response.body().toString());
                        /*Si la respuesta es ok cargo el cuerpo en mCartas y llamo a UpdateUI para actualizar el adapter del RecyclerView*/
                        mCartas = response.body();
                      updateUI(mCurrentPosition);

                    }
                    break;
                    case Constantes.NO_CONTENT_204:{
                        Toast.makeText(getContext(),getString(R.string.error_no_existe_carta),Toast.LENGTH_LONG).show();
                    }
                    break;
                }

        }}

        @Override
        public void onFailure(Call<List<Carta>> call, Throwable t) {
            Toast.makeText(getContext(),getString(R.string.error_problema_conexion),Toast.LENGTH_LONG).show();
        }
    }
    /**
     * Método para actualizar el adapter del RecyclerView
     * */
    public void updateUI( int position) {


        Log.i(TAG, "updateUI: "+mCartas);
                    if (mAdapter == null) {
                        Log.i(TAG, "onResponse: madapter == null");
                        mAdapter = new CartaAdapter(mCartas);
                        mCartaRecyclerView.setAdapter(mAdapter);
                    } else {
                        if (mCartas.size() > 0) {
                            Log.i(TAG, "onResponse if mcartas.size>0: "+position);
                            mAdapter.notifyItemChanged(position);
                        }
                        mAdapter.setCartas(mCartas);
                    }


                }


    /**
     * ViewHolder del RecyclerView, se encarga de manejar la vista de cada elemento del ReciclerView
     */

    protected class CartaHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.list_item_carta_nombre)
        TextView mListItemCartaNombre;
        private Carta mCarta_;

        public CartaHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            ButterKnife.bind(this,itemView);
        }

        public void bindCarta(Carta carta) {
            mCarta_ = carta;
            mListItemCartaNombre.setText(mCarta_.getNoombre());
        }

        @Override
        public void onClick(View view) {
            /*Si se pulsa sobre un elemento del RecyclerView acualizamos la variable con la posicion
            * y se llama al callback con la carta como argumento*/
            mCurrentPosition = getAdapterPosition();
            mListener.onCartaSelected(mCarta_);
        }
    }

    /**
     * Adapter para el RecyclerView facilita los datos para las vistas del RecyclerView
     */
    protected class CartaAdapter extends RecyclerView.Adapter<CartaHolder> {
        @BindView(R.id.list_item_carta_nombre)
        TextView mListItemCartaNombre;
        private List<Carta> mCartas;

        public CartaAdapter(List<Carta> cartas) {
            mCartas = cartas;
        }

        @Override
        public CartaHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            View v = layoutInflater.inflate(R.layout.list_item_carta, parent, false);
            ButterKnife.bind(this,v);
            return new CartaHolder(v);
        }

        @Override
        public void onBindViewHolder(CartaHolder holder, int position) {
            Log.i(TAG, "onBindViewHolder: position "+position);
            Carta carta = mCartas.get(position);
            holder.bindCarta(carta);
        }

        @Override
        public int getItemCount() {
            return mCartas.size();
        }

        public void setCartas(List<Carta> cartas) {
            mCartas = cartas;
        }
    }
}
