package com.juank.proyecto.controlador;


import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.juank.proyecto.R;
import com.juank.proyecto.Utilidades.Constantes;
import com.juank.proyecto.modelo.Notificacion;
import com.juank.proyecto.servicios.RestApiAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 *Fargment para la gestión de las notificaciones
 */
public class NotificacionesFragment extends Fragment {

    private static final String EMAIL = "email";
    @BindView(R.id.notificaciones_recycler_view)
    RecyclerView mNotificacionesRecyclerView;
    Unbinder unbinder;

    private NotificacionesAdapter mAdapter;
    private ProgressDialog mProgressDialog;
    private List<Notificacion> mLista;
    private String mIp;

    private String mEmail;


    public NotificacionesFragment() {
        // Required empty public constructor
    }

    /**
     * Método factory para instanciar un fragmento  y pasarle argumentos
     *
     * @param email String con el email del cliente
     * @return A new instance of fragment NotificacionesFragment.
     */

    public static NotificacionesFragment newInstance(String email) {
        NotificacionesFragment fragment = new NotificacionesFragment();
        Bundle args = new Bundle();
        args.putString(EMAIL, email);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mEmail = getArguments().getString(EMAIL);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_notificaciones, container, false);
        unbinder = ButterKnife.bind(this, view);
        Toolbar toolbar = getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.toolbar_notificaciones));
        mNotificacionesRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        cargaDatos();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        mIp=prefs.getString("ip","");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public void cargaDatos() {
        /*Obtengo la ip*/
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        mIp=prefs.getString("ip","");
        /*Preparo la llamada al REST*/
        Call<List<Notificacion>> call = RestApiAdapter.getApiService(mIp).listaNotificaciones(mEmail);
        /*Muestro el ProgressDialog*/
        mProgressDialog = new ProgressDialog(getContext());
        mProgressDialog.setTitle(R.string.progress_cargando);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setMax(100);
        mProgressDialog.show();
        /*encolo la llamada*/
        call.enqueue(new Callback<List<Notificacion>>() {
            @Override
            public void onResponse(Call<List<Notificacion>> call, Response<List<Notificacion>> response) {
                /*Si hay respuesta oculto el progressdialog*/
                mProgressDialog.dismiss();
                int responseCode = response.code();
                if (response.isSuccessful()) {
                    switch (responseCode){
                        case Constantes.OK_200:{
                            mLista = response.body();
                            mAdapter = new NotificacionesAdapter(mLista);
                            mNotificacionesRecyclerView.setAdapter(mAdapter);
                        }
                        break;
                    }
                }else if(responseCode == Constantes.NOT_FOUND_404){
                    Toast.makeText(getContext(),R.string.error_no_hay_notificacion, Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<List<Notificacion>> call, Throwable t) {
                mProgressDialog.dismiss();
                Toast.makeText(getContext(), R.string.error_problema_conexion, Toast.LENGTH_LONG).show();
            }
        });
    }


    protected class NotificacionesAdapter extends RecyclerView.Adapter<NotificacionesAdapter.NotificacionesHolder> {



        public NotificacionesAdapter(List<Notificacion> notificacionList) {
            mNotificacionList = notificacionList;
        }

        @BindView(R.id.textViewFecha)
        TextView mTextViewFecha;
        @BindView(R.id.textViewMensaje)
        TextView mTextViewMensaje;
        private List<Notificacion> mNotificacionList;


        @Override
        public NotificacionesHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_notificacion, parent, false);
            ButterKnife.bind(this, v);
            return new NotificacionesHolder(v);
        }

        @Override
        public void onBindViewHolder(NotificacionesHolder holder, int position) {
            Notificacion notificacion = mNotificacionList.get(position);
            holder.mTextViewFecha.setText(notificacion.getFecha().substring(0, 10));
            holder.mTextViewMensaje.setText(notificacion.getMensaje());
        }

        @Override
        public int getItemCount() {
            return mNotificacionList.size();
        }

        public void setNotificacionList(List<Notificacion> notificacionList) {
            mNotificacionList = notificacionList;
        }

        class NotificacionesHolder extends RecyclerView.ViewHolder {
            @BindView(R.id.textViewFecha)
            TextView mTextViewFecha;
            @BindView(R.id.textViewMensaje)
            TextView mTextViewMensaje;
            public NotificacionesHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }
        }
    }
}
