package com.juank.proyecto.controlador;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.juank.proyecto.R;
import com.juank.proyecto.Utilidades.Constantes;
import com.juank.proyecto.modelo.Reserva;
import com.juank.proyecto.modelo.SimpleResponse;
import com.juank.proyecto.servicios.RestApiAdapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * .   Fragment para la vista de solicitud de una reserva
 */
public class CrearReservaFragment extends Fragment implements AdapterView.OnItemSelectedListener {
    private static final String TAG = "CrearReserva";

    private static final int REQUEST_DATE = 0;
    private static final String DIALOG_DATE = "DialogDate";
    private static final String USER_PASS_PREFS = "user_pass_prefs";
    /*Vistas inyectadas con ButterKnife*/
    @BindView(R.id.textViewFecha)
    TextView mTextViewFecha;
    @BindView(R.id.etFecha)
    EditText mEtFecha;
    @BindView(R.id.radioButtonAlmuerzo)
    RadioButton mRadioButtonAlmuerzo;
    @BindView(R.id.radioButtonCena)
    RadioButton mRadioButtonCena;
    @BindView(R.id.radioGroup)
    RadioGroup mRadioGroup;
    @BindView(R.id.textViewHora)
    TextView mTextViewHora;
    @BindView(R.id.spinnerHora)
    Spinner mSpinnerHora;
    @BindView(R.id.textViewComensales)
    TextView mTextViewComensales;
    @BindView(R.id.etComensales)
    EditText mEtComensales;
    @BindView(R.id.btSolicitar)
    Button mBtSolicitar;
    Unbinder unbinder;
    /*Hora de llegada*/
    private String mHoraLlegada;
    /*email del cliente*/
    private String mEmail;
    /*id del cliente*/
    private int mIdCliente;
    /*IP del servidor*/
    private String mIp;
    /*Array con las horas a las que se puede reservar*/
    private String[] strHoras;
    /*Lista de horas*/
    private List<String> listaHoras;
    /*Adaptador para poblar el spinner de las horas*/
    private ArrayAdapter<String> comboAdapter;
    /*Progress dialog mientras se accede a los datos*/
    private ProgressDialog progressDoalog;
    /*Lista de reservas*/
    private List<Reserva> mListaReservas;


    public CrearReservaFragment() {
        // Required empty public constructor
    }

    /**
     * Método factory para crear una instacia de {@link CrearReservaFragment}
     *
     * @return A new instance of fragment CrearReservaFragment.
     */

    public static CrearReservaFragment newInstance() {
        CrearReservaFragment fragment = new CrearReservaFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences prefs = getActivity().getSharedPreferences(USER_PASS_PREFS, Context.MODE_PRIVATE);
        /*Al crear el fragment obtengo los datos del cliente de las preferencias, email e id*/
        mEmail = prefs.getString("username", "");
        mIdCliente = prefs.getInt("id", 0);

    }

    @Override
    public void onResume() {
        super.onResume();

        /*Cargo la ip del servidor de las preferencias.*/
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        mIp = prefs.getString("ip", "");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_crear_reserva, container, false);
        unbinder = ButterKnife.bind(this, view);
        /*configuracion del spinner*/
        setSpinner();
        /*Configuración del RadioButtonGroup*/
        setRadioButton();

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    /*onClickListener del campo fecha*/
    @OnClick(R.id.etFecha)
    public void onClickFecha() {
        Log.i(TAG, "onClickFecha: ");
        /*Al hacer tap en el campo fecha se abre un DiaologFragment para seleccionar fecha*/
        FragmentManager manager = getFragmentManager();
        /*Se le pasa el valor true como segundo parámetro apra indicar que el calendario no debe
         * mostrar dias anteriores ni posteriores a una semana*/
        DatePickerFragment dialog = DatePickerFragment.newInstance(new Date(), true);
        dialog.setTargetFragment(CrearReservaFragment.this, REQUEST_DATE);
        dialog.show(manager, DIALOG_DATE);
    }

    /*onClickListener para el boton solicitar*/
    @OnClick(R.id.btSolicitar)
    public void onClickSolicitar() {
        /*comprueba el campo comensales y que la fecha no este vacia, los otros dos campos cogen
         * valores por defecto  y no puede haber entrada errónea*/
        if (compruebaComensales(mEtComensales) && validaCampoVacio(mEtFecha)) {
            /*Carga las reservas del cliente y comprueba que no haya otra reserva para el mismo día
             * no es posible reservas 2 veces en el mismo día*/
            cargaReservas();
        }

    }

    /*Método para insertar la reserva a través del servicio REST*/
    private void insertaReserva() {
        Log.i(TAG, "insertaReserva: entra");
        String turno = null;
        int comensales = 0;
        /*Leo el radiobutton que esté seleccionado*/
        if (mRadioButtonAlmuerzo.isChecked()) {
            turno = "Almuerzo";
        } else if (mRadioButtonCena.isChecked()) turno = "Cena";
        /*la fecha y hora las tengo que mandar al REST en una sola cadena con el formato
         * yyyy/MM/dd HH:mm:ss así que cojo los datos de los 2 campos en la vista y los uno*/

        String fechaHora = new StringBuilder().append(mEtFecha.getText().toString())
                .append(" ")
                .append(mHoraLlegada).toString();

        comensales = Integer.parseInt(mEtComensales.getText().toString());

        /*Instacio el objeto call y lo encolo para que rettrofit haga la llamada al REST*/
        Call<SimpleResponse> call = RestApiAdapter.getApiService(mIp).insertaReserva(mIdCliente, turno, comensales, fechaHora);
        call.enqueue(new Callback<SimpleResponse>() {
            @Override
            public void onResponse(Call<SimpleResponse> call, Response<SimpleResponse> response) {
                if (response.isSuccessful()) {
                    /*si el código de respuesta está entre 200 - 400 es ok, se ha insertado si no
                     * muestro un mensaje de error*/
                    if (response.code() > 200 && response.code() < 399) {
                        Log.i(TAG, "onResponse inseta: " + response.code());
                        progressDoalog.dismiss();
                        Toast.makeText(getContext(), R.string.reserva_solicitada, Toast.LENGTH_LONG).show();
                    } else
                        Toast.makeText(getContext(), R.string.error_problema_conexion, Toast.LENGTH_LONG).show();

                }
            }

            @Override
            public void onFailure(Call<SimpleResponse> call, Throwable t) {
                progressDoalog.dismiss();
                Log.i(TAG, "onFailure: insertar" + t.getLocalizedMessage());
                Toast.makeText(getContext(), R.string.error_problema_conexion, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void cargaReservas() {
        Log.i(TAG, "cargaReservas: entra");
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        mIp=prefs.getString("ip","");
        /* ahora comprobamos que no haya ya una reser va con la misma fecha*/
        /*Solicito al servicio la lista de reservas del cliente*/
        Call<List<Reserva>> call = RestApiAdapter.getApiService(mIp).listaReservas(mEmail);
        progressDoalog = new ProgressDialog(getContext());
        progressDoalog.setMax(100);
        progressDoalog.setTitle(getString(R.string.progress_cargando));
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        // muestro el dialog
        progressDoalog.show();
        call.enqueue(new Callback<List<Reserva>>() {
            @Override
            public void onResponse(Call<List<Reserva>> call, Response<List<Reserva>> response) {
                String fecha = mEtFecha.getText().toString();
                Log.i(TAG, "onResponse: " + response.isSuccessful());
                int responseCode = response.code();
                if (response.isSuccessful()) {
                    Log.i(TAG, "onResponse: " + response.code());
                    switch (responseCode) {
                        /*si obtenemos una lista comparamos fechas y si ya existe reserva
                        en ese dia se muestra un mneasje de error*/
                        case Constantes.OK_200: {
                            mListaReservas = response.body();
                            Log.i(TAG, "onResponse: " + mListaReservas);
                            if (mListaReservas != null) {
                                boolean hayReserva = false;
                                for (Reserva r : mListaReservas) {
                                    String[] fechaHora = r.getFechaHora().split("-");
                                    Log.i(TAG, "onResponse: " + fechaHora[0]);
                                    Log.i(TAG, "onResponse: " + fecha);
                                    if (fechaHora[0].equals(fecha)) {
                                        hayReserva = true;
                                    }
                                }
                                if (hayReserva) {
                                    Log.i(TAG, "onClickSolicitar: hay reserva");
                                    progressDoalog.dismiss();
                                    Toast.makeText(getContext(), getString(R.string.error_existe_reserva), Toast.LENGTH_LONG).show();
                                } else {
                                    Log.i(TAG, "onClickSolicitar: no hay reserva");
                                    /*si no hay reserva se inserta*/

                                    insertaReserva();
                                }
                            }
                        }
                        break;

                    }


                } else {
                    /*Si el servidro responde 404 es que no hay reservas para ese cliente, entonces
                     * insertamos la nueva*/
                    switch (responseCode) {
                        case Constantes.NOT_FOUND_404: {
                            mListaReservas = null;
                            Log.i(TAG, "onResponse: 404");
                            insertaReserva();
                        }
                        break;
                    }

                }
            }

            @Override
            public void onFailure(Call<List<Reserva>> call, Throwable t) {
                progressDoalog.dismiss();
                Log.i(TAG, "onFailure: " + t.getLocalizedMessage());
                Toast.makeText(getContext(), getString(R.string.error_problema_conexion), Toast.LENGTH_LONG).show();
            }
        });
    }

    /*Método para comprobar que el número de coménsales está entre 1 y 10 y que no esté vacio*/
    private boolean compruebaComensales(EditText etComensales) {
        if (etComensales.getText().toString().isEmpty()) {
            etComensales.setError(getString(R.string.error_campo_requerido));
            return false;
        } else {
            int comensales = Integer.parseInt(etComensales.getText().toString());
            if (comensales < 1 || comensales > 10) {
                etComensales.setError(getString(R.string.error_max_comensales));
                return false;
            }
        }
        return true;
    }

    /**
     * Método para evaluar si un campo está vacio. Si está vacio devuelve false y pone un mensaje
     * de error en el campo.
     *
     * @param campo EditText del campo a evaluar
     * @return
     */
    private boolean validaCampoVacio(EditText campo) {
        if (campo.getText().toString().isEmpty()) {
            campo.setError(getString(R.string.error_campo_requerido));
            return false;
        }
        return true;
    }

    /*configurar el spinner*/
    private void setSpinner() {
        /*Configuración del spinner*/
        /*Seteo un listener para eschuchar los cambios en el spinner*/
        mSpinnerHora.setOnItemSelectedListener(this);
        /*Configuración del adapter para el spinner*/
        listaHoras = new ArrayList<>();
        strHoras = new String[]{"14:00", "14:15", "14:30", "14:45", "15:00", "15:15", "15:30"};
        Collections.addAll(listaHoras, strHoras);
        comboAdapter = new ArrayAdapter<>(getContext(), R.layout.support_simple_spinner_dropdown_item, listaHoras);
        mSpinnerHora.setAdapter(comboAdapter);
    }

    /*Configurar los RadioButton*/
    private void setRadioButton() {
        /*Dependiendo del que esté seleccionado se muestran unas hora u otras en el spinner*/
        mRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (i == R.id.radioButtonAlmuerzo) {
                    listaHoras = new ArrayList<>();
                    strHoras = new String[]{"14:00", "14:15", "14:30", "14:45", "15:00", "15:15", "15:30"};
                    Collections.addAll(listaHoras, strHoras);
                    comboAdapter = new ArrayAdapter<>(getContext(), R.layout.support_simple_spinner_dropdown_item, listaHoras);
                    mSpinnerHora.setAdapter(comboAdapter);
                } else if (i == R.id.radioButtonCena) {
                    listaHoras = new ArrayList<>();
                    strHoras = new String[]{"21:00", "21:15", "21:30", "21:45", "22:00", "22:15", "15:30"};
                    Collections.addAll(listaHoras, strHoras);
                    comboAdapter = new ArrayAdapter<>(getContext(), R.layout.support_simple_spinner_dropdown_item, listaHoras);
                    mSpinnerHora.setAdapter(comboAdapter);
                }
            }
        });
    }

    /**
     * Método para manejar la respuesta que se recibe del dialogo de selección de fecha
     *
     * @param requestCode Código de petición
     * @param resultCode  Código de resultado
     * @param data        datos del intent.
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i(TAG, "onActivityResult: " + resultCode);
        Log.i(TAG, "onActivityResult: " + requestCode);
        if (resultCode != Activity.RESULT_OK) {
            return;
        }
        if (requestCode == REQUEST_DATE) {
            String fecha = (String) data.getSerializableExtra(DatePickerFragment.EXTRA_DATE);
            Log.i(TAG, "onActivityResult: " + fecha);
            mEtFecha.setText(fecha);
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        switch (adapterView.getId()) {
            case R.id.spinnerHora:
                mHoraLlegada = strHoras[i];
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
        Toast.makeText(getContext(), "@string/crear_reservas_hora", Toast.LENGTH_SHORT);
    }


}
