package com.juank.proyecto.controlador;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessaging;
import com.juank.proyecto.R;
import com.juank.proyecto.servicios.RestApiAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * <p>
 * to handle interaction events.
 * Use the {@link SettingsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SettingsFragment extends PreferenceFragmentCompat {

    private static final String MENU = "menu";
    private static final String TEMPORADA = "temporada";
    private static final String PROMOCIONES = "promociones";
    private static final String IP="ip";

    private Snackbar snackBar;

    SharedPreferences.OnSharedPreferenceChangeListener listener = new SharedPreferences.OnSharedPreferenceChangeListener() {
        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
            if (s.equals(MENU)) {
                boolean opcion = sharedPreferences.getBoolean(MENU, true);
                if (opcion) {
                    FirebaseMessaging.getInstance().subscribeToTopic(MENU);

                    Toast.makeText(getActivity(), "Recibiras notificaciones cuando esté el menú del día", Toast.LENGTH_LONG).show();
                } else {
                    FirebaseMessaging.getInstance().unsubscribeFromTopic(MENU);
                    Toast.makeText(getActivity(), "No recibiras notificaciones cuando esté el menú del día", Toast.LENGTH_LONG).show();
                }

            } else if (s.equals(TEMPORADA)) {
                boolean opcion = sharedPreferences.getBoolean(TEMPORADA, true);
                if (opcion) {
                    FirebaseMessaging.getInstance().subscribeToTopic(TEMPORADA);
                } else FirebaseMessaging.getInstance().unsubscribeFromTopic(TEMPORADA);
            } else if (s.equals(PROMOCIONES)) {
                boolean opcion = sharedPreferences.getBoolean(PROMOCIONES, true);
                if (opcion) {
                    FirebaseMessaging.getInstance().subscribeToTopic(PROMOCIONES);
                } else FirebaseMessaging.getInstance().unsubscribeFromTopic(PROMOCIONES);
            }else if (s.equals(IP)){
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
                prefs.edit().putString(IP,sharedPreferences.getString(IP,"")).apply();
            }

        }
    };

    public SettingsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment SettingsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SettingsFragment newInstance() {
        SettingsFragment fragment = new SettingsFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }



    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        addPreferencesFromResource(R.xml.preferencias);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        preferences.registerOnSharedPreferenceChangeListener(listener);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = super.onCreateView(inflater, container, savedInstanceState);
        v.setBackgroundColor(getResources().getColor(android.R.color.white));
        v.setElevation(30);
        return v;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }



    @Override
    public void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(listener);
    }

    @Override
    public void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(listener);
    }


}
