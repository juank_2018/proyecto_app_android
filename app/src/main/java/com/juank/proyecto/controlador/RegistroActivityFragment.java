package com.juank.proyecto.controlador;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.juank.proyecto.R;
import com.juank.proyecto.Utilidades.Constantes;
import com.juank.proyecto.modelo.Cliente;
import com.juank.proyecto.servicios.RestApiAdapter;
import java.util.Date;
import java.util.regex.Pattern;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import static android.content.Context.MODE_PRIVATE;

/**
 * Fragmento para realizar el registro de la cuenta de los clientes.
 */
public final class RegistroActivityFragment extends Fragment {

    private static final String TAG="RegistroFragment";
    /*Constante para almacenar en las preferencias el token del dispositivo*/
    private static final String TOKEN_DISPOSITIVO= "token_dispositivo";
    private static final int REQUEST_DATE=0;
    private static final String DIALOG_DATE= "DialogDate";

    public static final String EXTRA_REGISTRO_EMAIL="com.juank.proyecto.registro_email";
    public  static final String EXTRA_REGISTRO_PASSWD="com.juank.proyecto.registro_passwd";
    public  static final String EXTRA_REGISTRO_ID="com.juank.proyecto.registro_id";

    @BindView(R.id.etEmail)
    EditText etEmail;
    @BindView(R.id.etContrasena)
    EditText etContrasena;
    @BindView(R.id.etContrasenaCon)
    EditText etContrasenaCon;
    @BindView(R.id.etNombre)
    EditText etNombre;
    @BindView(R.id.etApellido1)
    EditText etApellido1;
    @BindView(R.id.etApellido2)
    EditText etApellido2;
    @BindView(R.id.etFechaNacimiento)
    EditText etFechaNacimiento;
    @BindView(R.id.bRegistro)
    Button bRegistro;
    private String mIp;



    /**
     * constructor vacio
     */
    public RegistroActivityFragment() {
    }

    /**
     * Método del ciclo de vida del Fragment en el que se crea la vista.
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.fragment_registro, container, false);
        /*Inyección de las vistas*/
        ButterKnife.bind(this,v);


        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        mIp=prefs.getString("ip","");
    }

    /**
     * método onclick para el campo Fecha de nacimiento, abre un diálogo con un datepicker para
     * seleccionar la fecha.
     */
    @OnClick(R.id.etFechaNacimiento)
    public void onClickFecha(){

        FragmentManager manager = getFragmentManager();
        DatePickerFragment dialog = DatePickerFragment.newInstance(new Date(),false);
        dialog.setTargetFragment(RegistroActivityFragment.this,REQUEST_DATE);
        dialog.show(manager,DIALOG_DATE);
    }

    /**
     * método onclick para el botón de registro
     */
    @OnClick(R.id.bRegistro)
    public void onclick(){
        String email, contrasena, nombre, apellido1, apellido2,fecha;
        /*valido campos*/
        if (validaCampos()){
            /*si son validos extraigo el contenido en variables*/
            email=etEmail.getText().toString().trim();
            contrasena=etContrasena.getText().toString().trim();
            nombre=etNombre.getText().toString().trim();
            apellido1=etApellido1.getText().toString().trim();
            apellido2=etApellido2.getText().toString().trim();
            fecha=etFechaNacimiento.getText().toString().trim();
            /*obtengo el token de firebase para el dispositivo para almacenarlo en el servidor*/
            SharedPreferences prefs= getActivity().getSharedPreferences(TOKEN_DISPOSITIVO,MODE_PRIVATE);
            String token= prefs.getString("token","");
            Log.i(TAG, "onclick: token "+token);
            SharedPreferences pref1s = PreferenceManager.getDefaultSharedPreferences(getContext());
            mIp=pref1s.getString("ip","");


            /*ahora hacemos el POST al servicio REST*/
            Call<Cliente> call = RestApiAdapter.getApiService(mIp).registraCliente(email,contrasena,token,nombre,apellido1,apellido2,fecha);

            // Preparo un progressDialog para que se muestre mientras se hace la llamada al REST
            final ProgressDialog progressDoalog;
            progressDoalog = new ProgressDialog(getContext());
            progressDoalog.setMax(100);
            progressDoalog.setMessage(getString(R.string.progress_registro));
            progressDoalog.setTitle(getString(R.string.progress_registro_titulo));
            progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            // muestro el dialog
            progressDoalog.show();
            /*encolo la llamada de forma asincrona*/

            call.enqueue(new Callback<Cliente>() {


                @Override
                public void onResponse(Call<Cliente> call, Response<Cliente> response) {
                    /*Si hay respuesta oculto el progressDialog*/
                    progressDoalog.hide();
                    /*obtengo los datos del cliente y el codigo de respuesta*/
                    Cliente cliente=response.body();
                    int code = response.code();
                    if(response.isSuccessful()){
                        switch (code){
                            case Constantes.OK_200:{

                                Log.i(TAG, "onResponse: ok_200");
                                Toast.makeText(getContext(),R.string.registro_ok,Toast.LENGTH_SHORT).show();
                                /*Si la respuesta es ok meto el email y el passwd en un intent para
                                 * devolverlos a la actividad de login*/
                                setRegistradoResult(cliente.getEmail(),cliente.getPaswd(),cliente.getIdclientes());
                                getActivity().finish();
                            }
                            break;
                        }
                    }else {
                        switch (code){

                            case Constantes.CONFLICT_409:{
                                Log.i(TAG, "onResponse: conflict");
                                Toast.makeText(getContext(),R.string.registro_ko,Toast.LENGTH_SHORT).show();
                            }
                            break;
                        }
                    }

                }


                @Override
                public void onFailure(Call<Cliente> call, Throwable t) {
                    progressDoalog.hide();
                    Log.i(TAG, "onFailure: "+t.getLocalizedMessage());
                    Toast.makeText(getContext(),R.string.errorEnvioContrasena,Toast.LENGTH_SHORT).show();

                }
            });

        }
    }

    /**
     * Método para manejar la respuesta que se recibe del dialogo de selección de fecha
     * @param requestCode Código de petición
     * @param resultCode Código de resultado
     * @param data datos del intent.
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i(TAG, "onActivityResult: "+resultCode);
        Log.i(TAG, "onActivityResult: "+requestCode);
        if(resultCode != Activity.RESULT_OK){
            return;
        }
        if(requestCode == REQUEST_DATE){
            String fecha= (String) data.getSerializableExtra(DatePickerFragment.EXTRA_DATE);
            Log.i(TAG, "onActivityResult: "+fecha);
            etFechaNacimiento.setText(fecha);
        }
    }

    /**
     * Método para perparar los datos a devolver a la actividad de login
     * @param email correo electronico del cliente
     * @param passwd contraseña del cliente
     */
    private void setRegistradoResult(String email,String passwd,int id){
        Intent data = new Intent();
        data.putExtra(EXTRA_REGISTRO_EMAIL,email);
        data.putExtra(EXTRA_REGISTRO_PASSWD,passwd);
        data.putExtra(EXTRA_REGISTRO_ID,id);
        getActivity().setResult(Activity.RESULT_OK,data);
    }

    /**
     * Método que comprueba si una cadena está compuesta de 6 a 8 caracteres y tiene números y letras
     * @param contrasena Cadena a evaluar
     * @return True si la cadena es correcta
     */
    public boolean validaFortaleza(String contrasena){
        String patron="(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{6,8})$";
        return contrasena.matches(org.apache.commons.text.StringEscapeUtils.escapeJava(patron));
    }

    /**
     * Método para comprobar que una cadena es un email válido
     * @param email cadena a evaluar
     * @return true si la cadena es un email válido
     */
    public boolean validaEmail(String email){
        Log.i(TAG, "validaEmail: "+email);
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }

    /**
     * Método para validar los campos del formulario
     * @return true si todos los campos con correctos
     */
    private boolean validaCampos(){

        if (validaMail(etEmail) && validaContrasena(etContrasena)
                && validaContrasena(etContrasenaCon) && validaContrasenasIguales(etContrasena,etContrasenaCon)&&
                validaCampoVacio(etNombre)&& validaCampoVacio(etApellido1)&& validaCampoVacio(etApellido2)&& validaCampoVacio(etFechaNacimiento)){
            return true;
        }else return false;


    }

    /**
     * método para validar el campo email.Comprueba que el campo no esté vacio y en caso de no estarlo
     * que sea un mail válido.
     * @param etMail se le pasa el EditText del campo email
     * @return true si el texto del EditText es un mail correcto y no está vacio
     */
    protected boolean validaMail(EditText etMail){
        Log.i(TAG, "validaMail: "+etMail.getText().toString().trim());

        String mail=etMail.getText().toString().trim();
        /*Si está vacio devuelvo false y pongo mensaje de error*/
        if(etMail.getText().toString().isEmpty()){
            etMail.setError(getString(R.string.error_email_vacio));
            return false;
        }else{
            Log.i(TAG, "validaMail else: "+mail);
            /*Si no es un mail valido, false y mensaje de error*/
            if(!validaEmail(mail)){
                etMail.setError(getString(R.string.error_email));
                return false;
            }
        }
        return true;
    }

    /**
     * Método para comprobar que el campo contraseña es correcto. Comprueba que no esté vacio,
     * que no tenga más de 8 caracteres y que tenga números y letras
     * @param etContrasena Se le pasa el EditText del campo contraseña
     * @return true si cumple las condiciones
     */
    private boolean validaContrasena(EditText etContrasena){
        if(etContrasena.getText().toString().isEmpty()){
            etContrasena.setError(getString(R.string.error_passwd_min));
            return false;
        }else if(etContrasena.getText().length()>8){
            etContrasena.setError(getString(R.string.error_passwd_max));
            return false;
        }else if(!validaFortaleza(etContrasena.getText().toString().trim())){
            etContrasena.setError(getString(R.string.error_passwd_debil));
            return false;
        }
        return true;
    }

    /**
     * Método para evaluar si un campo está vacio. Si está vacio devuelve false y pone un mensaje
     * de error en el campo.
     * @param campo EditText del campo a evaluar
     * @return
     */
    private boolean validaCampoVacio (EditText campo){
        if(campo.getText().toString().isEmpty()){
            campo.setError(getString(R.string.error_campo_requerido));
            return false;
        }
        return true;
    }

    /**
     * Método que comprueba si 2 EditText tienen la misma cadena de texto, se usa para comprobar
     * que el campo contraseña y confirmar contraseña son iguales.
     * @param contrasena
     * @param contrasena2
     * @return true si son iguales.
     */
    private boolean validaContrasenasIguales(EditText contrasena,EditText contrasena2){
        return contrasena.getText().toString().equals(contrasena2.getText().toString());
    }
}
