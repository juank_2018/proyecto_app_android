package com.juank.proyecto.Utilidades;

import android.util.Patterns;

import java.util.regex.Pattern;

/**
 * Clase para definir algunas constantes
 */
public class Constantes {
/*Códigos de estado HTTP*/
    public static final int NOT_FOUND_404 = 404;
    public static final int NOT_AUTHORIZED_401 = 401;
    public static final int OK_200 = 200;
    public static final int CONFLICT_409=409;
    public static final int NO_CONTENT_204=204;
    public static final int SERVER_ERROR_500= 500;

    public static boolean validaEmail(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }
}
