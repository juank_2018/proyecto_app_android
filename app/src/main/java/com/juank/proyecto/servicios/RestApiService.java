package com.juank.proyecto.servicios;

import com.juank.proyecto.modelo.Carta;
import com.juank.proyecto.modelo.Cliente;
import com.juank.proyecto.modelo.Empleado;
import com.juank.proyecto.modelo.MenuREST;
import com.juank.proyecto.modelo.Notificacion;
import com.juank.proyecto.modelo.Reserva;
import com.juank.proyecto.modelo.SeccionCarta;
import com.juank.proyecto.modelo.SimpleResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Interface con la declaración de los métodos a usar por Retrofit para conectarse al servicio REST
 */

public interface RestApiService {

  @GET("cliente")
    Call<Cliente> getCliente(@Query(value = "email", encoded = true) String email,
                             @Query("passwd") String passwd,
                             @Query(value="token",encoded=true)String token);

  @GET("enviaContrasena")
  Call<SimpleResponse> enviaContrasena(@Query(value="email", encoded=true)String email);


  @POST("crearCliente")
  Call<Cliente> registraCliente(@Query(value="email",encoded = true) String email,
                                @Query(value="passwd",encoded=true) String passwd,
                                @Query(value="token",encoded=true)String dispositivo,
                                @Query(value="nombre",encoded = true)String nombre,
                                @Query(value="apellido1", encoded = true)String apellido1,
                                @Query(value="apellido2",encoded = true)String apellido2,
                                @Query(value="fecha",encoded = true)String fecha);

  @GET("carta")
  Call<List<Carta>> listaCartas();

  @GET("carta/{idCarta}")
  Call<List<SeccionCarta>> listaPlatosCarta(@Path(value="idCarta") int idCarta);
  @GET("menuPorFecha/{fecha}")
  Call<List<MenuREST>> listaPlatosMenu(@Path(value="fecha")String fecha);
  @GET("reservas")
  Call<List<Reserva>> listaReservas(@Query(value="email",encoded=true)String email);
  @POST("solicitaReserva")
  Call<SimpleResponse> insertaReserva(@Query(value="id")int id,
                               @Query(value="turno")String turno,
                               @Query(value="nComensales")int comensales,
                               @Query(value="fechaHora",encoded=true)String fechaHora);
  @GET("notificaciones")
  Call<List<Notificacion>> listaNotificaciones(@Query(value="email")String email);
}

