package com.juank.proyecto.servicios;

import android.app.Notification;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;
import android.view.View;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.juank.proyecto.R;

import butterknife.BindView;

/**
 * Clase para gestionar las notificaciones recibidas desde Firebase Cloud Messaging.
 * Si se recibe una notificación mientras la aplicación está cerrada o en backgroud se entrega en la
 * bandeja de sistema.
 * Si la notificación se recibe mientras la app está abierta entonces se ejecuta el método onMessageReceived
 * y genera una notificación en la aplicación
 */
public class MessagingService extends FirebaseMessagingService {
    @BindView(R.id.coordinator)
    CoordinatorLayout mCoordinator;

    public MessagingService() {
    }

    private static final String TAG = "servicio";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());

            Notification notification = new NotificationCompat.Builder(this)
                    .setContentTitle(remoteMessage.getNotification().getTitle())
                    .setContentText(remoteMessage.getNotification().getBody())
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .build();
            NotificationManagerCompat manager = NotificationManagerCompat.from(getApplicationContext());
            manager.notify(123, notification);
        }


    }
}
