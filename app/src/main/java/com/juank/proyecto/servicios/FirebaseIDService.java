package com.juank.proyecto.servicios;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Clase para obtener el token de la aplicacióm.
 * Cada vez que se instala la app en un dispositivo se genera un token en firebase que luego se utiliza
 * para las notificaciones.
 * Si se produce un cambio el método onTokenRefresh se ejecuta obteniendo el nuevo token
 * Created by jcpm0 on 16/03/2018.
 */

public class FirebaseIDService extends FirebaseInstanceIdService {
    private static final String TAG = "MyFirebaseIIDService";

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);


    }
}

