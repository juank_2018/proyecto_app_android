package com.juank.proyecto.servicios;


import android.util.Log;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Clase para gestionar las conexiones al servicio REST
 *
 */

public class RestApiAdapter {

    private static final String TAG = "ReatApi";


    private static RestApiService API_SERVICE;

    public static RestApiService getApiService(String ip) {
        Log.i(TAG, "getApiService: " + ip);
        String lastIp="";
        if(!ip.equals(lastIp)){
            lastIp=ip;
            API_SERVICE=null;
        }
        Log.i(TAG, "getApiService: "+lastIp);
        Log.i(TAG, "getApiService: "+API_SERVICE);
        // Creating the interceptor, and setting the log level
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        // add logging as last interceptor
        httpClient.addInterceptor(logging);  // <-- this is the important line!
        httpClient.connectTimeout(60, TimeUnit.SECONDS);
        httpClient.readTimeout(60, TimeUnit.SECONDS);
        httpClient.writeTimeout(60, TimeUnit.SECONDS);


        String baseUrl = "http://" + lastIp + ":8084/spring/REST/";
        Log.i(TAG, "getApiService: "+ baseUrl);
        if (API_SERVICE == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient.build()) // <-- using the log level
                    .build();
            API_SERVICE = retrofit.create(RestApiService.class);
        }

        return API_SERVICE;
    }

}

