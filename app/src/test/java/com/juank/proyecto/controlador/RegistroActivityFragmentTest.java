package com.juank.proyecto.controlador;

import android.widget.Button;
import android.widget.EditText;

import com.juank.proyecto.BuildConfig;
import com.juank.proyecto.R;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.support.v4.SupportFragmentTestUtil;

import butterknife.BindView;

import static org.junit.Assert.*;
@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class RegistroActivityFragmentTest {
    private RegistroActivity mRegistroActivity;
    private RegistroActivityFragment mRegistroActivityFragment;
    @Before
    public void setUp(){
        mRegistroActivity= Robolectric.buildActivity(RegistroActivity.class).create().resume().get();
        mRegistroActivityFragment=new RegistroActivityFragment();
        SupportFragmentTestUtil.startVisibleFragment(mRegistroActivityFragment);
        mRegistroActivityFragment.onAttachFragment(mRegistroActivityFragment);
    }
    @Test
    public void onCreateView() {

        EditText etEmail= (EditText)mRegistroActivity.findViewById(R.id.etEmail);
        assertNotNull(etEmail);
        EditText etContrasena=(EditText)mRegistroActivity.findViewById(R.id.etContrasena);
        assertNotNull(etContrasena);

        EditText etContrasenaCon=(EditText)mRegistroActivity.findViewById(R.id.etContrasenaCon);
        assertNotNull(etContrasenaCon);


        EditText etNombre=(EditText)mRegistroActivity.findViewById(R.id.etNombre);
        assertNotNull(etNombre);

        EditText etApellido1=(EditText)mRegistroActivity.findViewById(R.id.etApellido1);
        assertNotNull(etApellido1);
        EditText etApellido2=(EditText)mRegistroActivity.findViewById(R.id.etApellido2);
        assertNotNull(etApellido2);


        EditText etFechaNacimiento=(EditText)mRegistroActivity.findViewById(R.id.etFechaNacimiento);

        Button bRegistro=(Button)mRegistroActivity.findViewById(R.id.bRegistro);
        assertNotNull(bRegistro);
    }

    /**
     * test para el método validaFortaleza, el método devuelve true si la contraseña tiene
     * caracteres y números.
     */
    @Test
    public void validaFortalezaShoulRetrunTrue() {
        String passwd ="1234567u";
        assertTrue(mRegistroActivityFragment.validaFortaleza(passwd));
    }

    /**
     * test validaFortaleza con contraseña que no cumple requisitos, debe devolver dfalse
     */
    @Test
    public void validaFortalezaShoulRetrunFalse() {
        String passwd ="1234567";
        assertFalse(mRegistroActivityFragment.validaFortaleza(passwd));
    }

    /**
     * test del método validaEmail, devuelve true si se le pasa un email válido
     */
    @Test
    public void validaEMailShouldReturnTrue() {
        String email = "prueba2@gmail.com";

        assertTrue(mRegistroActivityFragment.validaEmail(email));
    }

    /**
     * test de validaEmail, el métdodo debe devolver false si se le pasa un email no válido
     */
    @Test
    public void validaEMailShouldReturnFalse() {
        String email = "prueba2@gmail";

        assertFalse(mRegistroActivityFragment.validaEmail(email));
    }

    /**
     * test del método validaMail, el método devuelve true si el campo email no está vacío y tiene
     * un email válido.
     */
    @Test
    public void validaMail() {
        mRegistroActivityFragment.etEmail.setText("jcpm06@gmail.com");
        assertTrue(mRegistroActivityFragment.validaMail(mRegistroActivityFragment.etEmail));

    }
    /**
     * test del método validaMail, el método devuelve true si el campo email no está vacío y tiene
     * un email válido.
     */
    @Test
    public void validaMailShouldReturnFalse() {

        assertFalse(mRegistroActivityFragment.validaMail(mRegistroActivityFragment.etEmail));

    }
}