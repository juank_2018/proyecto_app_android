package com.juank.proyecto.controlador;

import com.juank.proyecto.BuildConfig;
import com.juank.proyecto.modelo.Plato;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.support.v4.SupportFragmentTestUtil;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertNotNull;


@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class CartaDetalleFragmentTest {

    private CartaDetalleFragment mCartaDetalleFragment;
    @Before
    public void setUp(){

        mCartaDetalleFragment=CartaDetalleFragment.newInstance(new ArrayList<Plato>());
        SupportFragmentTestUtil.startVisibleFragment(mCartaDetalleFragment);

    }

    /**
     * test del método factory del fragment.
     * Debe devolver un fragment con los argumentos que se le pasan en el método.
     */
    @Test
    public void newInstance() {
        Plato plato = new Plato().withIdPlato(1).withNombre("prueba").withPrecioTapa(10.00);
        List<Plato> platos= new ArrayList<>();
        platos.add(plato);

        CartaDetalleFragment cartaDetalleFragment = CartaDetalleFragment.newInstance((ArrayList<Plato>) platos);
        assertNotNull(cartaDetalleFragment);
        assertNotNull(cartaDetalleFragment.getArguments().getParcelableArrayList("platos"));
    }


}