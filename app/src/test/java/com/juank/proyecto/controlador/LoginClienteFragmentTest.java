package com.juank.proyecto.controlador;

import android.content.Intent;
import android.support.v4.app.Fragment;

import com.juank.proyecto.BuildConfig;
import com.juank.proyecto.R;
import com.juank.proyecto.modelo.Cliente;
import com.juank.proyecto.servicios.RestApiService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.Shadows;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowActivity;
import org.robolectric.shadows.support.v4.SupportFragmentTestUtil;

import java.util.List;

import retrofit2.Call;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class LoginClienteFragmentTest {

    private static final String TAG = "LoginClienteFragmentTest";

    private LoginClienteFragment mLoginClienteFragment;
    private LoginCliente mLoginCliente;
    private SettingsFragment mSettingsFragment;
    private ShadowActivity mShadowActivity;
    @Before
    public void setUp() {
        mLoginCliente = Robolectric.buildActivity(LoginCliente.class).create().resume().get();
        mLoginClienteFragment = new LoginClienteFragment();
        mSettingsFragment= SettingsFragment.newInstance();
        SupportFragmentTestUtil.startVisibleFragment(mLoginClienteFragment);
        SupportFragmentTestUtil.startVisibleFragment(mSettingsFragment);
        mLoginCliente.onAttachFragment(mLoginClienteFragment);

        mShadowActivity = Shadows.shadowOf(mLoginCliente);
    }

    @Test
    public void shouldNotBeNull() {
        assertNotNull(mLoginClienteFragment);
    }

    @Test
    public void registrarTvClickShouldStartNewActivity() {
        mLoginClienteFragment.mTvRegistro.performClick();
        ShadowActivity.IntentForResult intent = Shadows.shadowOf(mLoginCliente).peekNextStartedActivityForResult();
        assertEquals(RegistroActivity.class.getCanonicalName(), intent.intent.getComponent().getClassName());
    }

    @Test
    public void recordarContrasenaTvClickShouldStartNewActivity() {
        mLoginClienteFragment.mTvContrasena.performClick();
        ShadowActivity.IntentForResult intent = Shadows.shadowOf(mLoginCliente).peekNextStartedActivityForResult();
        assertEquals(RecordarContrasena.class.getCanonicalName(), intent.intent.getComponent().getClassName());
    }



    @Test
    public void validaCamposShouldReturnTrue() {
        String email = "prueba2@gmail.com";
        String passwd = "1234567u";
        assertTrue(mLoginClienteFragment.validaCampos(email, passwd));
    }

    @Test
    public void validaCamposWhitEmptyFieldShuldReturnFalse() {
        String email = "";
        String passwd = "1234567u";
        assertFalse(mLoginClienteFragment.validaCampos(email, passwd));
    }

    @Test
    public void validaCamposWhitBadEmailShuldReturnFalse() {
        String email = "prueba2@gmail";
        String passwd = "1234567u";
        assertFalse(mLoginClienteFragment.validaCampos(email, passwd));
    }

    @Test
    public void validaMailShouldReturnTrue() {
        String email = "prueba2@gmail.com";

        assertTrue(mLoginClienteFragment.validaEmail(email));
    }

    @Test
    public void validaMailShouldReturnFalse() {
        String email = "prueba2@gmail";

        assertFalse(mLoginClienteFragment.validaEmail(email));
    }


}