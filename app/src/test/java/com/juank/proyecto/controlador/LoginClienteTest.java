package com.juank.proyecto.controlador;

import android.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import com.juank.proyecto.BuildConfig;
import com.juank.proyecto.R;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class LoginClienteTest {
private static final String TAG="loginTest";
    private LoginCliente mLoginCliente;
    private LoginClienteFragment mLoginClienteFragment;
    @Before
    public void setUp() throws Exception{
        mLoginCliente = Robolectric.buildActivity(LoginCliente.class).create().resume().get();
        mLoginClienteFragment=new LoginClienteFragment();

    }

    @Test
    public void shouldNotBeNull() throws Exception{
        assertNotNull(mLoginCliente);
    }

    @Test
    public void shouldHaveALoginClienteFragment()throws Exception{
        assertNotNull(mLoginCliente.getSupportFragmentManager().findFragmentById(R.id.fragment));
    }
}